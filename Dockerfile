FROM python:3.8-alpine

RUN apk add --no-cache build-base libffi-dev openssl-dev gettext doxygen graphviz

COPY docs/requirements-docs.txt docs/requirements-docs.txt

RUN pip install -r docs/requirements-docs.txt

