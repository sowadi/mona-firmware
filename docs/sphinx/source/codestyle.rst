Code Style Conventions
======================

To ensure a clean and readable code, we want to establish code style conventions.
Please follow these guidelines and only ignore them, when there is a good reason for it.

Use self explaining names and add comments, if there is more information necessary to understand
the code.

Indentation
-----------

The code should always be indented with 4 spaces.

Line length
-----------

The maximum line lenght should be around 90 characters. This is not a hard limit, but keeping
it in mind ensures better readability.


Function length
---------------

There is no hard limit for the number of lines in a function. Just make sure they are understandable.
Altough it is sometimes necessary, a function should not be so long, that it does not fit on screen.

Variables
---------

Words in variable names should be spaced with an underscore and use lower case letters.
Constants should be written in upper case letters.

.. code-block:: C

    int current_val = 1;
    const int MAX_VALUE = 255;

Classes
-------

Classes also use the underscore notation, but starting words with an upper case letter. To reduce
the mix-ups between member variables and local variables, member variables should end with
an underscore.

.. code-block:: C++
    :emphasize-lines: 3, 7

    class My_Class {
    private:
        int my_variable_;
    
    public:
        My_Class(int my_variable) {
            my_variable_ = my_variable;
        }
    }

Doc comments
------------

Doc comments should be written in the header file. Every class, function or member variable
should be documented.
Comments that explain the code in detail and are only of local interest,
should not be written as doc comments.

File names
----------

Files should be named in lowercase and with an underscore between words. For example `pin_layout.h`.
