.. MONA-Firmware documentation master file, created by
   sphinx-quickstart on Thu Aug 12 20:46:10 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MONA-Firmware's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   sensor

   mona_light_sleep

   mona_deep_sleep

   bg96

   gnss

   mona_gpio

   mona_file

   mona_time

   mona_scheduler

   log_data

   sys_event_log

   codestyle


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
