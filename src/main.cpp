
#ifdef ESP_PLATFORM
#include <Arduino.h>

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Adafruit_INA219.h>
#include <Adafruit_ADS1X15.h>
#include <FS.h>
#include <SD.h>
#include <SPI.h>
#include <WeatherStationDataRx.h>
#include <RTClib.h>
#include <WiFi.h>
#include "BLESerial.h"

#include "sys_event_log.h"
#include "mona_gpio.h"
#include "mona_file.h"
#include "mona_time.h"
#include "sensor.h"
#include "gnss.h"
#include "log_data.h"
#include "sys_event_log.h"
#include "mona_light_sleep.h"
#include "mona_deep_sleep.h"

Sensor_ADC adc(5000);
Adafruit_INA219 ina219;

struct timeval tv_last_average_scheduled; // is set in start_routine()
unsigned long average_time_ms = 60000;

char buf[128] = {0};

bool sleep_test = false;

void prepare_sleep(){
    rtc_gpio_set_level((gpio_num_t)RXB6_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)BG96_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)SD_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)T_DIS, HIGH);
    digitalWrite(RAIN_EN,LOW); //for some reason setting the RTC-GPIO does not work, but this does
    pin_lock_all(true);
}

/*
Method to print the reason by which ESP32
has been awaken from sleep
*/
void print_wakeup_reason(){
    esp_sleep_wakeup_cause_t wakeup_reason;

    wakeup_reason = esp_sleep_get_wakeup_cause();

    switch(wakeup_reason) {
        case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
        case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
        case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
        case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
        case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
        default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
    }
}

// gets executed once per day in the morning or whenever system is booted/reset
void setup() {
    init_gpios();

    // to be included if system logs should be shown in Serial Monitor
    init_sys_event_log();

    //Print the wakeup reason for ESP32
    print_wakeup_reason();

    //current sensor (INA219) has to be initialized to enter power saving mode:
    if(!ina219.begin()){
        SYS_EVENT_LOGE("INA219 not responding",buf);
    }else{
        ina219.powerSave(true);
    }

    adc.begin();

    // power health check
    prepare_sleep();    // create reproducible condition for voltage measurement
    delay(100);
    if(adc.battery_voltage()<3320){
        SYS_EVENT_LOGE("Emergency deep sleep due to low battery.",buf);
        prepare_sleep();
        esp_sleep_enable_timer_wakeup(10 * 1000000);
        esp_deep_sleep_start(); // should correspond to < 80uA 
    }
    pin_lock_all(false);

    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, HIGH);
    delay(1000);
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, LOW);
    
    GNSS gnss;
    gnss.query_gnss(500000);
    struct timeval tv = gnss.get_utc();
    
    MONA_Time *time = MONA_Time::get_instance();
    
    time->set_rtc(tv);
    adc.init();     // should be called after setting time

    struct timeval tv_current = time->get_current_time();
    DateTime dt = time->timeval_to_DateTime(tv_current);
    unsigned long ms_since_midnight = 3600000*dt.hour() + 60000*dt.minute() + 1000*dt.second();
    tv_last_average_scheduled.tv_sec = tv_current.tv_sec - (ms_since_midnight%average_time_ms)/1000;
    tv_last_average_scheduled.tv_usec = 0;
}

// gets executed continuously and fast, intermitted by short light sleeps until entering deep sleep for the day
void loop() {
    // measure adc data if scheduled 
    adc.service_routine();
    // average battery voltage measurements and log results to sd card
    // every full minute
    MONA_Time *time = MONA_Time::get_instance();
    struct timeval tv_current = time->get_current_time();
    struct timeval tv_next_average_scheduled;
    tv_next_average_scheduled.tv_sec = tv_last_average_scheduled.tv_sec + average_time_ms/1000;
    tv_next_average_scheduled.tv_usec = tv_last_average_scheduled.tv_usec + (average_time_ms%1000)*1000;
    if(later_than(tv_current, tv_next_average_scheduled)){
        
        Sample V_bat = adc.get_twa(VBAT, tv_last_average_scheduled, tv_next_average_scheduled);
        Sample T0 = adc.get_twa(CH0, tv_last_average_scheduled, tv_next_average_scheduled);
        Sample T1 = adc.get_twa(CH1, tv_last_average_scheduled, tv_next_average_scheduled);
        Sample T2 = adc.get_twa(CH2, tv_last_average_scheduled, tv_next_average_scheduled);
        Sample T3 = adc.get_twa(CH3, tv_last_average_scheduled, tv_next_average_scheduled);
        CONTI_Data data(V_bat.timestamp, V_bat.value, T0.value, T1.value, T2.value, T3.value);
        data.log();
        tv_last_average_scheduled = tv_next_average_scheduled;
        sleep_test = true;
    }
    
    MONA_Light_Sleep* light_sleep_instance = MONA_Light_Sleep::get_instance();
    MONA_Deep_Sleep* deep_sleep_instance = MONA_Deep_Sleep::get_instance();
    deep_sleep_instance->start();
    light_sleep_instance->start();
}

#else

int main() {
    return 1;
}

#endif

