# MONA-Firmware

The MONA-System is a Measuring System consisting of several off-grid measuring nodes that are ESP32-based and use a cellular module for remote data transmission.

If a new ESP32 FireBeetle is used with the MONA-Board it has to be registered first at IOT AppStory.com to enable over the air updates.
This is done by following along the guide: 