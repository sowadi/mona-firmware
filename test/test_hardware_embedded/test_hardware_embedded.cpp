#include <Arduino.h>
#include <unity.h>
#include <driver/rtc_io.h>
#include <BLEDevice.h>
#include "mona_gpio.h"
#include "pin_layout.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Adafruit_INA219.h>
#include <Adafruit_ADS1X15.h>
#include <RTClib.h>                     //documentation: https://adafruit.github.io/RTClib/html/index.html
#include "FS.h"
#include "SD_MMC.h"

Adafruit_BME280 bme;        // ambient sensor (hardware not native to board)
Adafruit_INA219 ina219;     // current sensor
Adafruit_ADS1115 ads_a;     // analogue to digital converters
Adafruit_ADS1115 ads_b;
RTC_PCF8563 rtc;            // real time clock

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("Failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println("Not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                //listDir(fs, file.path(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("  SIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
}

void createDir(fs::FS &fs, const char * path){
    Serial.printf("Creating Dir: %s\n", path);
    if(fs.mkdir(path)){
        Serial.println("Dir created");
    } else {
        Serial.println("mkdir failed");
    }
}

void removeDir(fs::FS &fs, const char * path){
    Serial.printf("Removing Dir: %s\n", path);
    if(fs.rmdir(path)){
        Serial.println("Dir removed");
    } else {
        Serial.println("rmdir failed");
    }
}

void readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\n", path);

    File file = fs.open(path);
    if(!file){
        Serial.println("Failed to open file for reading");
        return;
    }

    Serial.print("Read from file: ");
    while(file.available()){
        Serial.write(file.read());
    }
}

void writeFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
        Serial.println("File written");
    } else {
        Serial.println("Write failed");
    }
}

void appendFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        Serial.println("Failed to open file for appending");
        return;
    }
    if(file.print(message)){
        Serial.println("Message appended");
    } else {
        Serial.println("Append failed");
    }
}

void renameFile(fs::FS &fs, const char * path1, const char * path2){
    Serial.printf("Renaming file %s to %s\n", path1, path2);
    if (fs.rename(path1, path2)) {
        Serial.println("File renamed");
    } else {
        Serial.println("Rename failed");
    }
}

void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s\n", path);
    if(fs.remove(path)){
        Serial.println("File deleted");
    } else {
        Serial.println("Delete failed");
    }
}

void testFileIO(fs::FS &fs, const char * path){
    File file = fs.open(path);
    static uint8_t buf[512];
    size_t len = 0;
    uint32_t start = millis();
    uint32_t end = start;
    if(file){
        len = file.size();
        size_t flen = len;
        start = millis();
        while(len){
            size_t toRead = len;
            if(toRead > 512){
                toRead = 512;
            }
            file.read(buf, toRead);
            len -= toRead;
        }
        end = millis() - start;
        Serial.printf("%u bytes read for %u ms\n", flen, end);
        file.close();
    } else {
        Serial.println("Failed to open file for reading");
    }


    file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }

    size_t i;
    start = millis();
    for(i=0; i<2048; i++){
        file.write(buf, 512);
    }
    end = millis() - start;
    Serial.printf("%u bytes written for %u ms\n", 2048 * 512, end);
    file.close();
}


void setup()
{
    init_gpios();
    Serial.begin(115200);
    Serial.println("start");
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, HIGH);

    // ===== initialize I2C peripherals and set them to power saving modes (put into function?) =====

    // analoge to digital converters (2x ADS1115):
    ads_a.begin(0x48);        //seems like ADS1115 defaults to single-shot mode and should draw very little current (~1uA)
    ads_b.begin(0x49);        //seems like ADS1115 defaults to single-shot mode and should draw very little current (~1uA)
    ads_a.setGain(GAIN_ONE);
    ads_b.setGain(GAIN_ONE);
    
    // ambiance sensor (BME280):
    if(!bme.begin(0x76)){
        Serial.println("BME280 not responding");
    }else{
        bme.setSampling(Adafruit_BME280::MODE_SLEEP,
                        Adafruit_BME280::SAMPLING_X1, // temperature
                        Adafruit_BME280::SAMPLING_X1, // pressure
                        Adafruit_BME280::SAMPLING_X1, // humidity
                        Adafruit_BME280::FILTER_OFF   );
    }

    // current sensor (INA219):
    if(!ina219.begin()){
      Serial.println("INA219 not responding");
    }else{
      ina219.powerSave(true);
    }

     /* 
    ===== TESTING RAIN SENSOR =====
    Tree different states are possible:

                                    |  RAIN_EN-Pin |   RAIN-Pin   |   LED
    ------------------------------------------------------------------------
    1. Sensor is not active:      |     LOW      |     LOW      |   OFF         (regardless of how wet the sensor is)
    2. Sensor is active and dry:  |     HIGH     |     HIGH     |   OFF
    3. Sensor is active and wet:  |     HIGH     |     LOW      |   ON

    The sensor has a certain latency when switching between these states, that is a bit above 10ms. I suffices to wait 20ms to be sure to have a valid state.
    Thus to determine the wetness, following procedure is executed:
    */
    Serial.println("Enable rain sensor.");
    rtc_gpio_set_level((gpio_num_t)RAIN_EN,HIGH);     // enable sensor (pull RAIN_EN high)
    delay(20);                                        // wait a short amount of time (20ms is suffient)
    if(digitalRead(RAIN)){                            // check if RAIN gets pulled high(=dry) or not(=wet).
        Serial.println("RAIN-Pin got pulled HIGH. Thus the Sensor is dry.");
    }else{
        Serial.println("RAIN-Pin did not get pulled HIGH. Thus the Sensor is wet.");
    }                
    rtc_gpio_set_level((gpio_num_t)RAIN_EN,LOW);      // disable sensor (pull RAIN_EN low)
    Serial.println("Rain sensor disabled.");

    /*
    ===== TESTING REAL TIME CLOCK =====
    */
    if(rtc.begin()){
        Serial.println("Setting the real-time clock.");
        rtc.adjust(DateTime(2021,12,11,15,16,30));        // setting RTC arbitrarily to following date and time: 11.12.2021 15:16:30
        Serial.println("Reading from the real-time clock:");
        DateTime dt_now = rtc.now();
        char buf[] = "YY/MM/DD hh:mm:ss";
        Serial.println(dt_now.toString(buf));
    }else{
        Serial.println("There is a problem with the real-time clock.");
    }

    //===== TESTING SD CARD =====
    /*
    rtc_gpio_set_level((gpio_num_t)SD_EN, HIGH); // switch on power
    delay(1000);                                 // wait for a sufficient amount of time
    if(!SD_MMC.begin("/sdcard", true)){
        Serial.println("Card Mount Failed");
        return;
    } else {
        Serial.println("Card Mount Successful.");
    }

    uint8_t cardType = SD_MMC.cardType();
    if(cardType == CARD_NONE){
        Serial.println("No SD_MMC card attached");
        return;
    }

    Serial.print("SD_MMC Card Type: ");
    if(cardType == CARD_MMC){
        Serial.println("MMC");
    } else if(cardType == CARD_SD){
        Serial.println("SDSC");
    } else if(cardType == CARD_SDHC){
        Serial.println("SDHC");                     // this is the type we usually use
    } else {
        Serial.println("UNKNOWN");
    }

    uint64_t cardSize = SD_MMC.cardSize() / (1024 * 1024);
    Serial.printf("SD_MMC Card Size: %lluMB\n", cardSize);

    listDir(SD_MMC, "/", 0);
    createDir(SD_MMC, "/mydir");
    listDir(SD_MMC, "/", 0);
    removeDir(SD_MMC, "/mydir");
    listDir(SD_MMC, "/", 2);
    writeFile(SD_MMC, "/hello.txt", "Hello ");
    appendFile(SD_MMC, "/hello.txt", "World!\n");
    readFile(SD_MMC, "/hello.txt");
    deleteFile(SD_MMC, "/foo.txt");
    renameFile(SD_MMC, "/hello.txt", "/foo.txt");
    readFile(SD_MMC, "/foo.txt");
    testFileIO(SD_MMC, "/test.txt");
    Serial.printf("Total space: %lluMB\n", SD_MMC.totalBytes() / (1024 * 1024));
    Serial.printf("Used space: %lluMB\n", SD_MMC.usedBytes() / (1024 * 1024));
    rtc_gpio_set_level((gpio_num_t)SD_EN, LOW); // switch off power
    */

    // ===== CHECK RESPONSE OF I2C devices =====
    ina219.powerSave(false);
    Serial.printf("INA219: current = %.1f mA\n", ina219.getCurrent_mA());
    ina219.powerSave(true);

    bme.takeForcedMeasurement();
    Serial.printf("BME280: temperature = %.2f °C\n", bme.readTemperature());
    Serial.printf("BME280: humidity = %f percent RH\n", bme.readHumidity());
    Serial.printf("BME280: pressure = %f Pa\n", bme.readPressure());

    rtc_gpio_set_level((gpio_num_t)T_DIS,LOW);
    delay(100);

    Serial.printf("CH0: voltage = %.0f mV\n", ads_a.readADC_SingleEnded(0)*0.125);
    Serial.printf("CH1: voltage = %.0f mV\n", ads_a.readADC_SingleEnded(1)*0.125);
    Serial.printf("CH2: voltage = %.0f mV\n", ads_a.readADC_SingleEnded(2)*0.125);
    Serial.printf("CH3: voltage = %.0f mV\n", ads_a.readADC_SingleEnded(3)*0.125);

    Serial.printf("Input voltage = %.0f mV\n", ads_b.readADC_SingleEnded(0)*1.375);
    Serial.printf("Battery voltage = %.0f mV\n", ads_b.readADC_SingleEnded(1)*0.250);
    Serial.printf("3V3T voltage = %.0f mV\n", ads_b.readADC_SingleEnded(2)*0.125);
    Serial.printf("3V3 voltage = %.0f mV\n", ads_b.readADC_SingleEnded(3)*0.125);
   
    double val = ads_a.readADC_SingleEnded(0)*0.125;        // CH0 voltage
    double vref = ads_b.readADC_SingleEnded(2)*0.125;       // reference Voltage
    Serial.printf("3V3T voltage = %.0f mV\n", ads_b.readADC_SingleEnded(2)*0.125);
    double A = 3.9083*pow(10,-3);
    double B = -5.775*pow(10,-7);
    int R0 = 1000;
    val = val*1000.0/(vref-val);                            // resistance of RTD in Ohms
    val = -A/(2*B)-sqrt(pow(A/(2*B),2)+1/B*(val/R0-1));     // temperature in °C
    Serial.printf("CH0: temperature = %.2f °C\n", val);

    // =====

}

void loop()
{
    delay(1000);
}