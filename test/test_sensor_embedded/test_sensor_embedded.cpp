#include <Arduino.h>
#include <driver/rtc_io.h>

#include <SPI.h>
#include <BLEDevice.h>

#define _TIMERINTERRUPT_LOGLEVEL_ 4

#include "mona_gpio.h"
#include "pin_layout.h"
#include "mona_time.h"
#include "sensor.h"

Sensor_Value t;

unsigned long last_twa;

char buf[128] = {0};

const unsigned long samp_time_ms = 5000;
Sensor_ADC adc(samp_time_ms);
Sensor_Value pt1000;

unsigned long last_average = 0;

MONA_Time *time_instance;

void setup() {
    Serial.begin(115200);
    Serial.println("Start");
    
    init_gpios();
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, HIGH);
    delay(1000);
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, LOW);

    time_instance = MONA_Time::get_instance();
    const time_t unix_test_time{946724400}; //01.01.2000 11:00:00
    struct timeval tm_test_time = {unix_test_time, 0 };
    time_instance->set_rtc(tm_test_time);

    adc.begin();
    
    double test_values[] = {17.5, 17.7, 19.6, 17.5, NAN, 17.5, 17.3, 17.7, 1.6, 1.5};
    DateTime dt_test_time[10];

    dt_test_time[0] = DateTime(22,06,18,16,53,30);
    dt_test_time[1] = DateTime(22,06,18,16,53,40);
    dt_test_time[2] = DateTime(22,06,18,16,53,50);
    dt_test_time[3] = DateTime(22,06,18,16,54,0);
    dt_test_time[4] = DateTime(22,06,18,16,54,10);
    dt_test_time[5] = DateTime(22,06,18,16,54,20);
    dt_test_time[6] = DateTime(22,06,18,16,54,30);
    dt_test_time[7] = DateTime(22,06,18,16,54,40);
    dt_test_time[8] = DateTime(22,06,18,16,54,50);
    dt_test_time[9] = DateTime(22,06,18,16,55,0);

    DateTime dt_from = DateTime(22,06,18,16,53,0);
    struct timeval tv_from;
    tv_from.tv_sec = dt_from.unixtime();
    tv_from.tv_usec = 0;

    DateTime dt_to = DateTime(22,06,18,16,56,0);
    struct timeval tv_to;
    tv_to.tv_sec = dt_to.unixtime();
    tv_to.tv_usec = 0;
    
    
    
    sample_to_string(t.calc_twa(tv_from, tv_to),buf);
    Serial.print(buf);
    Serial.println("\t(Average)");
    memset(&buf[0], 0, sizeof(buf));

    
    t.clear_samples();
    for(int i=0; i<1; i++){
        struct timeval tv_test_time;
        tv_test_time.tv_sec = dt_test_time[i].unixtime();
        tv_test_time.tv_usec = 0;
        t.add_sample(test_values[i], tv_test_time);
    }
    
    t.get_string(0,buf);
    Serial.println(buf);
    memset(&buf[0], 0, sizeof(buf));

    sample_to_string(t.calc_twa(tv_from, tv_to),buf);
    Serial.print(buf);
    Serial.println("\t(Average)");
    memset(&buf[0], 0, sizeof(buf));


    t.clear_samples();
    for(int i=0; i<5; i++){
        struct timeval tv_test_time;
        tv_test_time.tv_sec = dt_test_time[i].unixtime();
        tv_test_time.tv_usec = 0;
        t.add_sample(test_values[i], tv_test_time);
    }
    sample_to_string(t.calc_twa(tv_from, tv_to),buf);
    Serial.print(buf);
    Serial.println("\t(Average)");
    memset(&buf[0], 0, sizeof(buf));

    t.clear_samples();
    for(int i=0; i<10; i++){
        struct timeval tv_test_time;
        tv_test_time.tv_sec = dt_test_time[i].unixtime();
        tv_test_time.tv_usec = 0;
        t.add_sample(test_values[i], tv_test_time);
    }
    sample_to_string(t.calc_twa(tv_from, tv_to),buf);
    Serial.print(buf);
    Serial.println("\t(Average)");
    memset(&buf[0], 0, sizeof(buf));
}

void loop()
{
    adc.service_routine();
    /** Note:
    * The service routine above checks on its own if preperation or sampling is due. (minimalistic variant)
    * It is however sufficient, if 'adc.service_routine' is only called shortly after 'adc.get_next_schedule'
    * as is implemented in the following:
    */
    /*
    timeval tv_current = time_instance->get_current_time();
    timeval tv_next = adc.get_next_schedule();
    if(later_than(tv_current, tv_next)){
        adc.service_routine();  // will be called in alternating intervalls of 1ms an 5000ms
    }
    */

    if(millis()-last_average > 30000){
        last_average = millis();
        Serial.println("Buffered Data:");
        pt1000 = adc.get_raw_data(CH0);
        for(int i=0; i<TIMESERIES_LEN; i++){
            pt1000.get_string(i,buf);
            Serial.println(buf);
            memset(&buf[0], 0, sizeof(buf));
        }
        sample_to_string(adc.get_twa(CH0,30),buf);
        Serial.printf("Time Weighted Average:\n%s\n",buf);
        memset(&buf[0], 0, sizeof(buf));
    }
}