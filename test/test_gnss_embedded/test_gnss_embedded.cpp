
#include <unity.h>
#include <string.h>
#include <Arduino.h>
#include <RTClib.h>

#include "mona_gpio.h"
#include "pin_layout.h"

#define protected public
#define private public
#include "gnss.h"
#undef protected
#undef private

void test_query_gnss(void)
{
    GNSS gnss;
    TEST_ASSERT_FALSE(gnss.success_);
    gnss.query_gnss();
    TEST_ASSERT_EQUAL_CHAR_ARRAY("\r\n+QGPSLOC: ", gnss.response_, 11);
    TEST_ASSERT_TRUE(gnss.success_);
}

void test_parse_response(void)
{
    GNSS gnss;
    strcpy(gnss.response_, "+QGPSLOC: 201752.0,49.88562,8.67008,4.8,129.0,2,0.00,0.0,0.0,140221,02");
    gnss.parse_response();
    TEST_ASSERT_EQUAL(49.88562, gnss.get_latitude());
    TEST_ASSERT_EQUAL(8.67008, gnss.get_longitude());
    DateTime dt_time = {gnss.get_utc().tv_sec};
    TEST_ASSERT_EQUAL(14, dt_time.day());
    TEST_ASSERT_EQUAL(2, dt_time.month());
    TEST_ASSERT_EQUAL(2021, dt_time.year());
    TEST_ASSERT_EQUAL(20, dt_time.hour());
    TEST_ASSERT_EQUAL(17, dt_time.minute());
    TEST_ASSERT_EQUAL(52, dt_time.second());
}

void setup() {

    init_gpios();
    gpio_set_level((gpio_num_t)STATUSLIGHT, HIGH);

    UNITY_BEGIN();
    RUN_TEST(test_parse_response);
    RUN_TEST(test_query_gnss);
    UNITY_END();
}

void loop() {
    delay(500);
}