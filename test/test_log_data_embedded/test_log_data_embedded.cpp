
#include <SPI.h>
#include <BLEDevice.h>

#include <unity.h>
#include <string.h>
#include <stdio.h>

#include "mona_file.h"
#include "mona_gpio.h"

#define private public
#define protected public
#include "log_data.h"
#undef private
#undef protected


void setUp(void)
{
    const char *filename = "/test.csv";
    MONA_File file;
    file.delete_file(filename);
}


void tearDown(void)
{
    const char *filename = "/test.csv";
    MONA_File file;
    file.delete_file(filename);
}


void test_conti_data_in_string_out() {
    const char *filename = "/test.csv";
    struct timeval tv = {1660407065, 0};
    CONTI_Data data(tv, 2210);
    char out[data.row_len_bytes] = {0};
    data.to_string(out);
    char expected[] = "1660407065,2.21V\n";
    TEST_ASSERT_EQUAL_STRING(expected, out);
    TEST_ASSERT_EQUAL_INT(sizeof(expected), data.row_len_bytes);
}


// void test_sylog_data_in_string_out() {
//     const char *filename = "/test.csv";
//     SYLOG_Data data(1, 21);
//     char out[data.row_len_bytes] = {0};
//     data.to_string(out);
//     char expected[] = "1,21\n";
//     TEST_ASSERT_EQUAL_STRING(expected, out);
//     TEST_ASSERT_EQUAL_INT(sizeof(expected), data.row_len_bytes);
// }


void test_write_read_to_mona_file() {
    const char *filename = "/test.csv";
    MONA_File file;
    char expected[] = "1,2,3\n";
    TEST_ASSERT_TRUE(file.append(filename, expected));
    char written[7] = {0};
    file.read_line(filename, written, 0, 6);
    TEST_ASSERT_EQUAL_STRING(expected, written);
    file.delete_file(filename);
}


void test_read_from_middle_of_mona_file() {
    const char *filename = "/test.csv";
    MONA_File file;
    file.append(filename, "2,2,2\n");
    char expected[] = "1,2,3\n";
    file.append(filename, expected);
    file.append(filename, "2,2,2\n");
    char written[7] = {0};
    file.read_line(filename, written, 6, 6);
    TEST_ASSERT_EQUAL_STRING(expected, written);
    file.delete_file(filename);
}

// void test_logger() {
//     CONTI_Data data(1, 2.1);
//     TEST_ASSERT_TRUE(data.log());
//     data.delete_logfile((struct timeval){0, 0});
//     SYLOG_Data sydata(1, 21);
//     TEST_ASSERT_TRUE(sydata.log());
//     sydata.delete_logfile((struct timeval){0, 0});
// }

void test_date_string() {
    struct timeval tv = {1657809389, 0};
    CONTI_Data data(tv, 0.1);
    char date[11] = {0};
    data.to_date_string(date, tv);
    TEST_ASSERT_EQUAL_STRING("2022-07-14", date);
    

    struct timeval tv2 = {315529200, 0}; // 1980-01-01 0:0:0
    CONTI_Data data2(tv2, 0.1);
    char date2[11] = {0};
    data2.to_date_string(date2, tv2);
    TEST_ASSERT_EQUAL_STRING("1980-01-01", date2);
}

void setup() {
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);

    init_gpios();

    UNITY_BEGIN();
    RUN_TEST(test_conti_data_in_string_out);
    // RUN_TEST(test_sylog_data_in_string_out);
    RUN_TEST(test_write_read_to_mona_file);
    RUN_TEST(test_read_from_middle_of_mona_file);
    // RUN_TEST(test_logger);
    UNITY_END();
}


void loop() {
    delay(500);
}
