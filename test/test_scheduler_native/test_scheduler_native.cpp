
#include <unity.h>
#include <sys/time.h>

#include "mona_scheduler.h"
#include "mona_time.h"
#include "bg96.h"

// necessary for native tests that run on windows:
void setUp (void) {}
void tearDown (void) {}

typedef struct {
    unsigned long millis;
    unsigned long last_millis;
    int day_of_week;
    tm datetime;
} BG96_Schedule_Data;


class Sens : public MONA_Scheduler
{
public:
    struct timeval get_next_schedule(void *schedule_data) {
        BG96_Schedule_Data *data = (BG96_Schedule_Data *)schedule_data;
        tm last = data->datetime;
        last.tm_min += 5;
        timeval tv_time = {0, 0};
        tv_time.tv_sec = mktime(&last);
        return tv_time;
    }
};


struct timeval bg96_scheduler(void *schedule_data) {
    BG96_Schedule_Data *data = (BG96_Schedule_Data *)schedule_data;
    tm last = data->datetime;
    last.tm_sec += 5;
    timeval tv_time = {0, 0};
    tv_time.tv_sec = mktime(&last);
    return tv_time;
}

void test_bg96_scheduler(void) {
    BG96 bg96;

    bg96.set_schedule_function(bg96_scheduler);

    struct tm dt = {};
    dt.tm_year=2022; dt.tm_mon=3; dt.tm_mday=30;

    BG96_Schedule_Data data = {
        2000ul,
        1000ul,
        1,
        dt,
    };
    struct timeval result = bg96.get_next_schedule(&data);
    struct tm res = *(localtime(&result.tv_sec));
    TEST_ASSERT_EQUAL_INT64((long)mktime(&dt)+5, (long)mktime(&res));
}

void test_sens_scheduler(void) {
    Sens sens;


    struct tm dt = {};
    dt.tm_year=2022; dt.tm_mon=3; dt.tm_mday=30;

    BG96_Schedule_Data data = {
        2000ul,
        1000ul,
        1,
        dt,
    };
    struct timeval result = sens.get_next_schedule(&data);
    struct tm res = *(localtime(&result.tv_sec));

    TEST_ASSERT_EQUAL_INT64((long)mktime(&dt)+5*60, (long)mktime(&res));
}

int main(int argc, char **argv) {

    UNITY_BEGIN();
    RUN_TEST(test_bg96_scheduler);
    RUN_TEST(test_sens_scheduler);
    UNITY_END();

    return 0;
}
