#include <Arduino.h>
#include <unity.h>
#include "SPI.h"
#include "BLESerial.h"
#include "mona_gpio.h"
#include "mona_time.h"

// void test_tm_to_DateTime(void)
// {
//     const time_t unix_test_time { 946724400 };
//     struct tm tm_time = *localtime(&unix_test_time);
//     DateTime dt_time{unix_test_time};

//     DateTime converted_dt_time{};
//     TEST_ASSERT_EQUAL_STRING(, )
// }
MONA_Time *time_instance;

void test_set_get_rtc (void)
{
    const time_t unix_test_time{946724400}; //01.01.2000 11:00:00
    struct timeval tm_test_time = {unix_test_time, 0 };
    time_instance->set_rtc(tm_test_time);
    struct timeval time_after_set = time_instance->get_current_time();
    TEST_ASSERT_EQUAL_INT32(tm_test_time.tv_sec, time_after_set.tv_sec);
    // TEST_ASSERT_EQUAL_INT32(tm_test_time.tv_usec, time_after_set.tv_usec);
    // TEST_ASSERT_EQUAL_INT32(tm_test_time.tm_year, time_after_set.tm_year);
    // TEST_ASSERT_EQUAL_INT32(tm_test_time.tm_hour, time_after_set.tm_hour);
    // TEST_ASSERT_EQUAL_INT32(tm_test_time.tm_min, time_after_set.tm_min);
    //TEST_ASSERT_EQUAL_INT32(tm_test_time.tm_sec, time_after_set.tm_sec);
}

    void
    setup()
{
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);

    init_gpios(); 

    time_instance = MONA_Time::get_instance();
    UNITY_BEGIN();
    RUN_TEST(test_set_get_rtc);
    UNITY_END();
}

void loop()
{
    delay(500);
}