#include <unity.h>
#include <string.h>
#include <stdio.h>

#include "log_data.h"
#include "mona_file.h"

// necessary for native tests that run on windows:
void setUp (void) {
    MONA_File file;
    file.delete_file("test.csv");
}
void tearDown (void) {
    MONA_File file;
    file.delete_file("test.csv");
}

void test_conti_data_in_string_out() {
    CONTI_Data data((struct timeval){1, 0}, 2.1);
    char out[data.row_len_bytes] = {0};
    data.to_string(out);
    char expected[] = "1,2.1\n";
    TEST_ASSERT_EQUAL_STRING(expected, out);
    TEST_ASSERT_EQUAL_INT(sizeof(expected), data.row_len_bytes);
}


void test_sylog_data_in_string_out() {
    SYLOG_Data data((struct timeval){1, 0}, 21);
    char out[data.row_len_bytes] = {0};
    data.to_string(out);
    char expected[] = "1,21\n";
    TEST_ASSERT_EQUAL_STRING(expected, out);
    TEST_ASSERT_EQUAL_INT(sizeof(expected), data.row_len_bytes);
}


void test_write_read_to_mona_file() {
    MONA_File file;
    char expected[] = "1,2,3\n";
    file.append("test.csv", expected);
    char written[7] = {0};
    file.read_line("test.csv", written, 0, 7);
    TEST_ASSERT_EQUAL_STRING(expected, written);
    file.delete_file("test.csv");
}


void test_read_from_middle_of_mona_file() {
    MONA_File file;
    file.append("test.csv", "2,2,2\n");
    char expected[] = "1,2,3\n";
    file.append("test.csv", expected);
    file.append("test.csv", "2,2,2\n");
    char written[7] = {0};
    file.read_line("test.csv", written, 6, 7);
    TEST_ASSERT_EQUAL_STRING(expected, written);
    file.delete_file("test.csv");
}


// void test_logger1() {
//     Data_Logger logger;
//     CONTI_Data data(1, 2.1);
//     TEST_ASSERT_TRUE(logger.log(&data));
//     logger.delete_logfile(1, CONTI);
//     Data_Logger sylogger;
//     SYLOG_Data sydata(1, 21);
//     TEST_ASSERT_TRUE(sylogger.log(&sydata));
//     sylogger.delete_logfile(1, SYLOG);
// }

void test_logger() {
    CONTI_Data data((struct timeval){1, 0}, 2.1);
    TEST_ASSERT_TRUE(data.log());
    data.delete_logfile((struct timeval){0, 0});
    SYLOG_Data sydata((struct timeval){1, 0}, 21);
    TEST_ASSERT_TRUE(sydata.log());
    sydata.delete_logfile((struct timeval){0, 0});
}

// TODO: read the logged line!

int main(int argc, char **argv) {

    UNITY_BEGIN();
    RUN_TEST(test_conti_data_in_string_out);
    RUN_TEST(test_sylog_data_in_string_out);
    RUN_TEST(test_write_read_to_mona_file);
    RUN_TEST(test_read_from_middle_of_mona_file);
    RUN_TEST(test_logger);
    UNITY_END();

    return 0;
}
