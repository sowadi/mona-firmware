#include <Arduino.h>
#include <unity.h>
#include <driver/rtc_io.h>
#include <BLEDevice.h>
#include "mona_gpio.h"
#include "pin_layout.h"
#include <jled.h>

#define FLUSH_START_CONDITION_WINDOW    2000
#define FLUSH_STOP_CONDITION_WINDOW     2000

bool flushing = false, flush_active = false ,  flowing = false;
unsigned long flow_start, flush_start, flow_stop, flow_duration;

JLed status_indicator = JLed(STATUSLIGHT).Off();

/*
  ISR for the flush-sensor
*/
void IRAM_ATTR flush_isr(){
    if(gpio_get_level((gpio_num_t)FLUSH)==LOW){
        if(!flushing){
            flush_start = millis(); // reference time for start of flush
        } 
        flow_start = millis();
        flush_active = true;
        flowing = true;
        status_indicator = JLed(jled::Esp32Hal(STATUSLIGHT, 7, 5000)).Blink(1,39).MaxBrightness(100).Forever();
    }
    else{
        if(flowing){
            flow_stop = millis();
            flow_duration = flow_duration + flow_stop - flow_start;
            flowing = false;
            status_indicator = JLed(jled::Esp32Hal(STATUSLIGHT, 7, 5000)).Off();
        }
    }
}

/*
  Flush routine that uses the infos the flush ISR provides, evaluates the flush and loggs it to the SD-card
*/
void flush_routine(){
    if(flush_active && flowing && ((flow_duration+millis()-flow_start) > FLUSH_START_CONDITION_WINDOW)){
        flushing = true;
    }
    if(flush_active && flushing && !flowing && ((millis()-flow_stop) > FLUSH_STOP_CONDITION_WINDOW)){
        Serial.printf("Flow-Duration = %2.3f seconds\n",(float)flow_duration/1000);
        flush_active = false;
        flushing = false;
        flow_duration = 0;
    }
    else if(flush_active && !flushing && !flowing && ((millis()-flow_stop) > FLUSH_STOP_CONDITION_WINDOW)){
        Serial.printf("Flush event not recognized as actual Flush: FLUSH_START_CONDITION_WINDOW = %d seconds not reached\n",FLUSH_START_CONDITION_WINDOW/1000);
        flush_active = false;
        flushing = false;
        flow_duration = 0;
    }
    status_indicator.Update();
}

void setup()
{
    init_gpios();
    Serial.begin(115200);
    Serial.println("start");
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, HIGH);
    rtc_gpio_set_level((gpio_num_t)FLUSH_EN, HIGH);
    delay(1000);
    attachInterrupt(digitalPinToInterrupt(FLUSH),flush_isr,CHANGE);
}

void loop()
{
    delay(10);
    flush_routine();
}