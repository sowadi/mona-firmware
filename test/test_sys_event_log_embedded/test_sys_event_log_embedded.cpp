#include <Arduino.h>
#include <unity.h>
#include "SPI.h"
#include "BLEDevice.h"
#include "mona_gpio.h"
#include "sys_event_log.h"
#include "FS.h"
#include "SD_MMC.h"
#include "pin_layout.h"
#include "driver/rtc_io.h"

void test_sd_card_logging(void)
{

    MONA_File file;
    file.delete_file(LOG_FILE_PATH);
    //log below buffer limit
    for (int i = 0; i < 2; i++)
    {
        SYS_EVENT_LOGI("******TESTNACHRICHT******"); // 25 chars
    }
    // TEST_ASSERT_EQUAL_INT32(0, file.file_size(LOG_FILE_PATH));
    //check if logfile empty / doesn't exist because buffer limit should not be reached yet
    TEST_ASSERT_FALSE(SD_MMC.exists(LOG_FILE_PATH));
    //write enough logs so flush_buffer() is executed
    for (int i = 0; i < 40; i++)
    {
        SYS_EVENT_LOGI("******TESTNACHRICHT******"); // 25 chars
    }
    //check if logs written to file
    TEST_ASSERT_GREATER_THAN_INT32(3000, file.file_size(LOG_FILE_PATH));
    TEST_ASSERT_LESS_THAN_INT32(4000, file.file_size(LOG_FILE_PATH));

    file.delete_file(LOG_FILE_PATH);
}


void setup()
{
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);
    init_sys_event_log();
    init_gpios();
    rtc_gpio_set_level((gpio_num_t)SD_EN, HIGH);
    delayMicroseconds(1000);
    SD_MMC.begin("/sdcard", true);
    UNITY_BEGIN();
    RUN_TEST(test_sd_card_logging);
    UNITY_END();
}

void loop()
{
    delay(500);
}