#ifdef ESP_PLATFORM
#include <Arduino.h>
#include <driver/adc.h>

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Adafruit_INA219.h>
#include <Adafruit_ADS1X15.h>
#include <FS.h>
#include <SD.h>
#include <SPI.h>
#include <WeatherStationDataRx.h>
#include <RTClib.h>
#include <WiFi.h>
#include "BLESerial.h"

#include "mona_gpio.h"
#include "mona_file.h"
#include "mona_time.h"
#include "sensor.h"
#include "gnss.h"
#include "log_data.h"

Adafruit_INA219 ina219;

void start_routine()
{
    init_gpios();
    //current sensor (INA219) has to be initialized to enter power saving mode:
    if(!ina219.begin()){
        Serial.println("INA219 not responding");
    }else{
        ina219.powerSave(true);
    }

    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, HIGH);
    delay(1000);
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, LOW);
}


void setup() {
    start_routine();
    delay(15000);       // should correspond to ca. 45mA

    //prepare deep sleep
    rtc_gpio_set_level((gpio_num_t)RXB6_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)BG96_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)SD_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)T_DIS, HIGH);
    digitalWrite(RAIN_EN,LOW); //for some reason setting the RTC-GPIO does not work, but this does
    pin_lock_all(true);

    esp_sleep_enable_timer_wakeup(15 * 1000000);
    esp_light_sleep_start(); // should correspond to < 1.9mA

    esp_sleep_enable_timer_wakeup(15 * 1000000);
    esp_deep_sleep_start(); // should correspond to < 80uA 

}

void loop() {}
#else

int main() {
    return 1;
}

#endif