#include <Arduino.h>
#include <unity.h>
#include <driver/rtc_io.h>
#include <BLEDevice.h>
#include "mona_gpio.h"
#include "pin_layout.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Adafruit_INA219.h>
#include <Adafruit_ADS1X15.h>


/**
 * This is a sketch to test the charging of the Li-Ion Battery from the solar cell.
 * It might be a good idea to do this test with each MONA before deployment to avoid power issues.
 * For this purpose the INA219 current sensor on the MONA-Board is used. (special cable required)
 * The charging current can not be logged via serial because the power supply via the USB cable
 * would spoil the measurement of the charge current.
 * The current version is thus not usable.
 * @todo implement the sketch below with logging to SD-card instead of using serial
*/

Adafruit_INA219 ina219;

void print_current(){
  ina219.powerSave(false);
  Serial.printf("INA219: current = %.1f mA\n", ina219.getCurrent_mA());
  ina219.powerSave(true);
}

void setup()
{
    init_gpios();
    Serial.begin(115200);
    Serial.println("start");
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, HIGH);
    //current sensor (INA219):
    if(!ina219.begin()){
      Serial.println("INA219 not responding");
    }else{
      ina219.powerSave(true);
    }
}

void loop()
{
    delay(1000);
    print_current();
}