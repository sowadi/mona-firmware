#include <SPI.h>
#include <BLEDevice.h>
#include <Arduino.h>
#include <unity.h>


#include "mona_file.h"
#include "mona_time.h"
#include "mona_file.h"
#include "mona_gpio.h"
#include "pin_layout.h"
#include "sys_event_log.h"


void test_read_write_over_sdmmc(void) {
    MONA_File file;
    const char expected[] = "1,2,3\n";
    const char *filename = "/test.csv";
    file.delete_file(filename);
    file.append(filename, expected);
    file.append(filename, expected);
    char written[7] = {0};
    TEST_ASSERT_TRUE(file.read_line(filename, written, 0, 6));
    TEST_ASSERT_EQUAL_STRING(expected, written);
    file.delete_file(filename);
}


void test_read_from_middle_of_mona_file() {
    MONA_File file;
    const char *filename = "/test.csv";
    file.delete_file(filename);
    TEST_ASSERT_TRUE(file.append(filename, "1,2,3\n"));
    TEST_ASSERT_TRUE(file.append(filename, "4,5,6\n"));
    TEST_ASSERT_TRUE(file.append(filename, "7,8,9\n"));
    TEST_ASSERT_TRUE(file.append(filename, "0,0,0\n"));
    char written[7] = {0};
    // test reading in different order than writing
    file.read_line(filename, written, 6, 6);
    TEST_ASSERT_EQUAL_STRING("4,5,6\n", written);
    file.read_line(filename, written, 0, 6);
    TEST_ASSERT_EQUAL_STRING("1,2,3\n", written);
    file.read_line(filename, written, 18, 6);
    TEST_ASSERT_EQUAL_STRING("0,0,0\n", written);
    file.delete_file(filename);
}

void test_power_off_and_on() {
    {
        MONA_File file;
        const char *filename = "/test.csv";
        file.delete_file(filename);
        file.append(filename, "1,2,3\n");
    }

    {
        MONA_File file;
        const char *filename = "/test.csv";
        char written[7] = {0};
        file.read_line(filename, written, 0, 6);
        TEST_ASSERT_EQUAL_STRING("1,2,3\n", written);
        file.delete_file(filename);
    }
}

void test_power_off_for_2_instances() {
    const char *filename = "/test.csv";
    MONA_File file1;

    {
        MONA_File file2;

        file2.delete_file(filename);

        TEST_ASSERT_TRUE(file2.append(filename, "1,2,3\n"));
    }
    char out[7] = {0};
    TEST_ASSERT_TRUE(file1.read_line(filename, out, 0, 7));
    TEST_ASSERT_EQUAL_STRING("1,2,3\n", out);
    file1.delete_file(filename);
}


void write1Task(void *pvParameters) {
    MONA_File file;
    for (int i = 0; i < 8; i++) {
        vTaskDelay(500);
        file.append("/test.csv", "1,1,1\n");
    }
    for(;;){vTaskDelay(500);}
}


void write2Task(void *pvParameters) {
    MONA_File file;
    for (int i = 0; i < 8; i++) {
        vTaskDelay(500);
        file.append("/test.csv", "2,2,2\n");
    }
    for(;;){vTaskDelay(500);}
}


void test_parallel_writes() {

    {
        // clean up and power off
        MONA_File file;
        file.delete_file("/test.csv");
    }

    BaseType_t xReturned;
    TaskHandle_t xTask1 = NULL;
    TaskHandle_t xTask2 = NULL;


    xReturned = xTaskCreate(
        write1Task,
        "Write 1 Task",
        configMINIMAL_STACK_SIZE * 10,
        (void *) 1,
        tskIDLE_PRIORITY,
        &xTask1
    );


    xReturned = xTaskCreate(
        write2Task,
        "Write 2 Task",
        configMINIMAL_STACK_SIZE * 10,
        (void *) 2,
        tskIDLE_PRIORITY,
        &xTask2
    );


    vTaskDelay(8000/portTICK_PERIOD_MS);


    vTaskDelete(xTask1);
    vTaskDelete(xTask2);

    MONA_File file;
    const char *filename = "/test.csv";

    char out[7] = {0};

    for (int i = 0; i < (2 * 8); i++) {
        TEST_ASSERT_TRUE(file.read_line(filename, out, i * 6, 6));
        if (out[0] == '1') {
            TEST_ASSERT_EQUAL_STRING("1,1,1\n", out);
        } else {
            TEST_ASSERT_EQUAL_STRING("2,2,2\n", out);
        }
    }
    file.delete_file(filename);
}


// TODO: test reading from a line, that does not exist!


void setup() {
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);

    Serial.begin(115200);

    init_gpios();

    UNITY_BEGIN();
    RUN_TEST(test_read_write_over_sdmmc);
    RUN_TEST(test_read_from_middle_of_mona_file);
    RUN_TEST(test_power_off_for_2_instances);
    RUN_TEST(test_power_off_and_on);
    RUN_TEST(test_parallel_writes);
    UNITY_END();
}

void loop() {
    delay(500);
}

