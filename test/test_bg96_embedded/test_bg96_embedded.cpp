
#include <Arduino.h>
#include <unity.h>

#include <driver/rtc_io.h>
#include "bg96.h"
#include "mona_gpio.h"
#include "pin_layout.h"


void test_at(void) {
    BG96 bg = BG96::get_instance();
    char response[256] = {0};
    TEST_ASSERT_TRUE(bg.send_AT_cmd("AT", "OK", response));
    TEST_ASSERT_EQUAL_STRING("\r\nOK", response);
}


void test_wrong_response(void) {
    BG96 bg = BG96::get_instance();
    TEST_ASSERT_FALSE(bg.send_AT_cmd("AT", "WRONG"));
}


void test_singleton(void) {
    BG96 bg1 = BG96::get_instance();
    BG96 bg2 = BG96::get_instance();
    TEST_ASSERT_EQUAL_HEX32(&bg1, &bg2); // TODO: fails, but works when using normal execution in main.cpp?
}


void setup() {
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);

    init_gpios();
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, HIGH); // TODO: is a function `enable_lights()` useful?

    BG96 bg = BG96::get_instance();
    bg.power_on();

    UNITY_BEGIN();
    // RUN_TEST(test_singleton);
    RUN_TEST(test_at);
    RUN_TEST(test_wrong_response);
    UNITY_END();
}


void loop() {
    delay(500);
}
