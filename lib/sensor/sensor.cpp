#include "sensor.h"
#include "mona_light_sleep.h"
#include "sys_event_log.h"

unsigned long diff_ms(timeval t1, timeval t2)
{
    if(later_than(t1,t2)){
        if(t1.tv_usec>t2.tv_usec){
            return ((t1.tv_sec - t2.tv_sec) * 1000) + ((t1.tv_usec - t2.tv_usec)/1000);
        }else{
            return ((t1.tv_sec - t2.tv_sec) * 1000) - ((t2.tv_usec - t1.tv_usec)/1000);
        }
    }else{
        return 0;
    }
}

unsigned long diff_us(timeval t1, timeval t2)
{
    if(later_than(t1,t2)){
        if(t1.tv_usec>t2.tv_usec){
            return ((t1.tv_sec - t2.tv_sec) * 1000000) + ((t1.tv_usec - t2.tv_usec));
        }else{
            return ((t1.tv_sec - t2.tv_sec) * 1000000) - ((t2.tv_usec - t1.tv_usec));
        }
    }else{
        return 0;
    }
}

bool later_than(timeval a, timeval b){
    if(a.tv_sec > b.tv_sec){
        return true;
    }else if(a.tv_sec == b.tv_sec && a.tv_usec > b.tv_usec){
        return true;
    }else{
        return false;
    }
}

void sample_to_string(Sample samp, char *buf){
    char temp_buf[64];
    MONA_Time::timeval_to_String(samp.timestamp, temp_buf);
    sprintf(buf, "value: %.1f\t%s", samp.value, temp_buf);
}

Sample nan_sample(){
    Sample nan_return;
    nan_return.value = NAN;
    nan_return.timestamp.tv_sec = 0;
    nan_return.timestamp.tv_usec = 0;
    return nan_return;
}

void Sensor_Value::clear_samples(){
    for(int i = 0; i < TIMESERIES_LEN; i++){
        timeseries_[i].value = NAN;
        // TODO clear timestamp aswell
    }
}

void Sensor_Value::add_sample(double value, struct timeval timestamp){
    for(int i = TIMESERIES_LEN - 1; i > 0; i--){
        timeseries_[i] = timeseries_[i-1];
    }
    Sample s;
    s.value = value;
    s.timestamp = timestamp;
    timeseries_[0] = s;
}

Sample Sensor_Value::get_sample(int idx){
    if(idx >= TIMESERIES_LEN){
        return nan_sample();
    }
    return timeseries_[idx];
}


void Sensor_Value::get_string(int idx, char *buf){
    sample_to_string(get_sample(idx), buf);
}

Sample Sensor_Value::calc_twa(timeval from, timeval to){
    unsigned long total_duration = diff_ms(to, from);
    if(total_duration <= 0){
        // TODO: throw warning
        return nan_sample();
    }
    timeval t_value_left = from;
    timeval t_value_right = t_value_left;
    double twa = NAN;

    for(int i=TIMESERIES_LEN-1; i>=0; i--){
        Sample current_sample = get_sample(i);
        timeval current = current_sample.timestamp;
        if(later_than(current, from) && later_than(to, current) && !isnan(current_sample.value)){
            for(int j=i-1; j>=0; j--){
                Sample next_sample = get_sample(j);
                timeval next = next_sample.timestamp;
                if(later_than(next,from) && later_than(to, next) && !isnan(next_sample.value)){
                    t_value_right.tv_sec = 0.5*current.tv_sec + 0.5*next.tv_sec;
                    t_value_right.tv_usec = 0.5*current.tv_usec + 0.5*next.tv_usec;
                    break;
                }
            }
            if(t_value_left.tv_sec == t_value_right.tv_sec && t_value_left.tv_usec == t_value_right.tv_usec){
                t_value_right = to;
            }
            unsigned long duration = diff_ms(t_value_right,t_value_left);
            if(isnan(twa)){
                twa = double(duration)/double(total_duration)*get_sample(i).value;
            }else{
                twa += double(duration)/double(total_duration)*get_sample(i).value;
            }
            t_value_left = t_value_right;
        }
    }
    Sample result;
    result.value = twa;
    result.timestamp.tv_sec = 0.5*from.tv_sec + 0.5*to.tv_sec;
    result.timestamp.tv_usec = 0.5*from.tv_usec + 0.5*to.tv_usec;
    return result;
}

Sample Sensor_Value::calc_twa(unsigned long since_sec){
    MONA_Time* time_instance = MONA_Time::get_instance();
    struct timeval tv_current = time_instance->get_current_time();
    timeval tv_from = tv_current;
    tv_from.tv_sec -= since_sec;
    return calc_twa(tv_from, tv_current);
}

void Sensor_ADC::begin(){
    // analoge to digital converters (2x ADS1115):
    adc_a.begin(0x48);        //seems like ADS1115 defaults to single-shot mode and should draw very little current (~1uA)
    // TODO: The following line produces a warning: "[  2123][W][Wire.cpp:204] begin(): Bus already started in Master Mode."
    adc_b.begin(0x49);        //seems like ADS1115 defaults to single-shot mode and should draw very little current (~1uA)
    adc_a.setGain(GAIN_ONE);
    adc_b.setGain(GAIN_ONE);
    this->init();
}

void Sensor_ADC::init(){
    MONA_Time* time_instance = MONA_Time::get_instance();
    struct timeval tv_current = time_instance->get_current_time();
    tv_current.tv_sec += 1;
    tv_current.tv_usec = 0;
    DateTime dt = time_instance->timeval_to_DateTime(tv_current);
    unsigned long ms_since_midnight = 3600000*dt.hour() + 60000*dt.minute() + 1000*dt.second();
    last_scheduled_.tv_sec = tv_current.tv_sec - (ms_since_midnight%samp_time_ms_)/1000;
    last_scheduled_.tv_usec = 0;
    // Note: it is not ensured, that last_scheduled_ is always in the past!

    this->state_=STANDBY;
    last_state_change_ = tv_current;
}

void Sensor_ADC::enable(){
    rtc_gpio_set_level((gpio_num_t)T_DIS,LOW);
}

void Sensor_ADC::disable(){
    rtc_gpio_set_level((gpio_num_t)T_DIS,HIGH);
}


void Sensor_ADC::take_sample(){
    char buf[128] = {0};
    MONA_Time* time_instance = MONA_Time::get_instance();
    struct timeval tv_timestamp = time_instance->get_current_time();
    double value;
    byte channel[4] = {CH0, CH1, CH2, CH3};
    SYS_EVENT_LOGV("Sampling from ADC Sensor (CH0-CH3, VIN, VBAT, V3_3, V3_3T):");
    for(int i=0; i<4; i++){
        switch (channel_config_[channel[i]])
        {
        case VOLTAGE:
            value = NAN;
            break;

        case PT1000:
            value = pt1000_temperature(channel[i]);
            break;

        case CURRENT:
            value = NAN;
            break;

        default:
            value = NAN;
            break;
        }
        adc_value_[channel[i]].add_sample(value, tv_timestamp);
        memset(&buf[0], 0, sizeof(buf));
        Sample sample_temp = adc_value_[channel[i]].get_sample(0);
        sample_to_string(sample_temp, buf);
        SYS_EVENT_LOGV(buf);
    }
    adc_value_[VIN].add_sample(input_voltage(), tv_timestamp);
    memset(&buf[0], 0, sizeof(buf));
    Sample sample_temp = adc_value_[VIN].get_sample(0);
    sample_to_string(sample_temp, buf);
    SYS_EVENT_LOGV(buf);

    adc_value_[VBAT].add_sample(battery_voltage(), tv_timestamp);
    memset(&buf[0], 0, sizeof(buf));
    sample_temp = adc_value_[VBAT].get_sample(0);
    sample_to_string(sample_temp, buf);
    SYS_EVENT_LOGV(buf);

    adc_value_[V3_3].add_sample(voltage(V3_3), tv_timestamp);
    memset(&buf[0], 0, sizeof(buf));
    sample_temp = adc_value_[V3_3].get_sample(0);
    sample_to_string(sample_temp, buf);
    SYS_EVENT_LOGV(buf);

    adc_value_[V3_3T].add_sample(voltage(V3_3T), tv_timestamp);
    memset(&buf[0], 0, sizeof(buf));
    sample_temp = adc_value_[V3_3T].get_sample(0);
    sample_to_string(sample_temp, buf);
    SYS_EVENT_LOGV(buf);
}

double Sensor_ADC::pt1000_temperature(byte channel_id){
    if(channel_id >= CH0 && channel_id <= CH3){
        double val = adc_a.readADC_SingleEnded(channel_id)*0.125;
        double vref = adc_b.readADC_SingleEnded(V3_3T-4)*0.125;
        double A = 3.9083*pow(10,-3);
        double B = -5.775*pow(10,-7);
        double R0 = 1000.0;
        val = val*1000.0/(vref-val);                            // resistance of RTD in Ohms
        val = -A/(2*B)-sqrt(pow(A/(2*B),2)+1/B*(val/R0-1));     // temperature in °C
        if(val<-99 || val>999){
            val = NAN;
        }
        return val;
    }else{
        return NAN;
    }
}

double Sensor_ADC::input_voltage(){
    return adc_b.readADC_SingleEnded(VIN-4)*1.375;
}

double Sensor_ADC::battery_voltage(){
    return adc_b.readADC_SingleEnded(VBAT-4)*0.250;
}

double Sensor_ADC::voltage(byte channel_id){
    if(channel_id >= 0 && channel_id < 4){
        return adc_a.readADC_SingleEnded(channel_id)*0.125;
    }else if(channel_id >= 4 && channel_id < 8){
        return adc_b.readADC_SingleEnded(channel_id-4)*0.125;
    }else{
        return NAN;
    }
}

bool Sensor_ADC::service_routine(){
    byte state = state_;
    MONA_Time* time_instance = MONA_Time::get_instance();
    MONA_Light_Sleep* light_sleep = MONA_Light_Sleep::get_instance();
    struct timeval tv_timestamp = time_instance->get_current_time();
    timeval tv_next = this->get_next_schedule();
    light_sleep->update_wake_up_time(tv_next);
    if(later_than(tv_timestamp, tv_next)){
        switch (state)
        {
        case PREPARING:
            take_sample();
            disable();
            last_state_change_ = tv_timestamp;
            state_ = STANDBY;
            light_sleep->decrement_veto();
            break;
        case STANDBY:
            last_scheduled_ = get_next_schedule();
            state_ = PREPARING;
            light_sleep->increment_veto();
            enable();
            last_state_change_ = tv_timestamp;
            break;
        }
        return true;
    }else{
        return false;
    }
}

struct timeval Sensor_ADC::get_next_schedule(void *schedule_data){
    MONA_Time* time_instance = MONA_Time::get_instance();
    struct timeval tv_current = time_instance->get_current_time(); 

    struct timeval tv_next;        

    switch (state_)
    {
    case STANDBY:
        tv_next.tv_sec = last_scheduled_.tv_sec + samp_time_ms_/1000;
        tv_next.tv_usec = last_scheduled_.tv_usec + (samp_time_ms_%1000)*1000;
        break;
    case PREPARING:
        tv_next.tv_sec = last_state_change_.tv_sec + preparing_time_us_/1000000;
        tv_next.tv_usec = last_state_change_.tv_usec + preparing_time_us_%1000000;
        break;
    
    default:
        // Add warning, that adc might be uninitialized.
        break;
    }
    return tv_next;
}

Sensor_Value Sensor_ADC::get_raw_data(byte channel_id){
    return adc_value_[channel_id];
}

Sample Sensor_ADC::get_twa(byte channel_id, timeval interval_from, timeval interval_to){
    return adc_value_[channel_id].calc_twa(interval_from, interval_to);
}

Sample Sensor_ADC::get_twa(byte channel_id, int since_seconds){
    return adc_value_[channel_id].calc_twa(since_seconds);
}
