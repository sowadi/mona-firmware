#pragma once

#include <Arduino.h>
#include <driver/rtc_io.h>
#include <time.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADS1X15.h>

#include "pin_layout.h"
#include "mona_time.h"
#include "mona_scheduler.h"

/* Constants */
#define TIMESERIES_LEN 15

/* do not change these! (correspond to channel numbers of ADCs) */
#define CH0             0
#define CH1             1
#define CH2             2
#define CH3             3
#define VIN             4
#define VBAT            5
#define V3_3T           6       // corresponds to netlabel "3V3T"
#define V3_3            7       // corresponds to netlabel "3V3"

#define VOLTAGE         0
#define PT1000          1
#define CURRENT         2

#define UNINITIALIZED   0
#define STANDBY         1
#define PREPARING       2

/** TODO:
 * - maybe add properties: quantity, unit
 * - write calc_max (see below)
 */

/**
 * @brief Basic tuple containing a sensor value and timespamp.
 */
typedef struct {
    double value;
    timeval timestamp;
} Sample;

unsigned long diff_ms(timeval t1, timeval t2);

bool later_than(timeval a, timeval b);

/**
 * @brief fill provided buffer with string representing the provided Sample
 * 
 * @param samp provided Sample
 * @param buf pointer to suffiently long char array
*/
void sample_to_string(Sample samp, char *buf);

/**
 * @brief Saves timeseries data as they are sampled with a specified buffer length. 
 * 
 * Datapoints are saved as Samples. 
 * They can be accessed individually, or using basic statistical evaluations. 
 */
class Sensor_Value{
private:
    Sample timeseries_[TIMESERIES_LEN];
public:
    /**
     * @brief Clears all sampled values
     */
    void clear_samples();

    Sensor_Value(){
        clear_samples();
    };

    /**
     * @brief Shifts all samples in timeseries, adding a new one, deleting the last one.
     * @param Value of quantity and corresponding timestamp
     */
    void add_sample(double value, struct timeval timestamp);

    /**
     * @brief Gets the specifically indexed sample.
     * 
     * @param Index inside buffer (0 = most recent, TIME_SERIES_LEN-1 = oldest)
     * @return Sample at indes location (value and timestamp)
     */
    Sample get_sample(int idx);

    /**
     * @brief fill provided buffer buf with string representing a specific Sample from the timeseries
     * 
     * @param idx index of specific sample.
     * @param buf pointer to suffiently long char array
     */
    void get_string(int idx, char *buf);

    /**
     * @brief Calculates the time weighted average (twa) of a time series.
     * @param Beginning and end ("from" and "to") of desired time interval to be averaged over
     * @return Sample containing twa (.value) and timestamp equidistant to "from" and "to".
     * The value of the returned sample is
     * - nan if no value or only nan values are available
     * - a number value if at least one non nan value is available, ignoring all nan values
     * The weight of each contributing sample value is determined by the time intervals to
     * the sample before and after, respectively. For the first and last sample the intervals
     * to the averaging bounds are considered.
     */
    Sample calc_twa(timeval from, timeval to);

    /**
     * @brief Calculates the time weighted average (twa) of a time series.
     * @param Time interval in seconds describing the averaging time intervall till the present time.
     * @return Sample containing twa (.value) and timestamp equidistant to "from" and "to".
     * The value of the returned sample is
     * - nan if no value or only nan values are available
     * - a number value if at least one non nan value is available, ignoring all nan values
     * The weight of each contributing sample value is determined by the time intervals to
     * the sample before and after, respectively. For the first and last sample the intervals
     * to the averaging bounds are considered.
     */
    Sample calc_twa(unsigned long since_sec);

    /**
     * @todo calculate maximum (e.g. for wind gust), similar as with mean.
     */
    Sample calc_max(timeval interval_from, timeval interval_to);

    Sample calc_max(int since_seconds);
};


/**
 * @brief Class for the ananloge to digital converters.
 * 
 * The object has a schedule that alternates between preparation and taking samples.
 * The sampled values are stored in Sensor_Value objects.
 * To collect samples: call method 'service_routine()' repeatedly and very fast, or based
 * on method 'get_next_schedule()'. A state machine inside this object takes care of the sampling.
 * Time weightend average values based on the buffered values can be obtained using the
 * methods 'get_twa'.
 * 
 * @todo: It might make sense to move some functionalities to a parent class in close to mid future.
 */
class Sensor_ADC : public MONA_Scheduler
{
private:
    unsigned long samp_time_ms_;
    const unsigned long preparing_time_us_ = 100;
    
    Adafruit_ADS1115 adc_a;     // analogue to digital converters
    Adafruit_ADS1115 adc_b;
    
    byte channel_config_[4] = {PT1000, PT1000, PT1000, PT1000};
    Sensor_Value adc_value_[8];

    struct timeval last_scheduled_;
    struct timeval last_state_change_;
    byte state_;

    /** 
     * @brief turn on needed power supply
     */
    void enable();

    /**
     * @brief measure sensor values and save Sensor_Value objects
     * 
     * gets current values for all channels of ADC sensors and calculates 
     * values of interest based on hardware configuration. 
     * Adds obtained values to the corresponding Sensor_Value objects for channel id
     */
    void take_sample();

    /**
     * @brief turn off power supply
     */
    void disable();

    /**
     * @brief get value for pt1000 temperature connected to ADC
     * 
     * extracts value of ADC at channel_id and calculates corresponding temperature
     * with the used voltage devider configuration on MONA Board 
     * 
     * @param channel_id channel_id corresponding to ADC device
     * @return double measured temperature in °C
     */
    double pt1000_temperature(byte channel_id);

    /**
     * @brief get value for voltage measured at ADC
     * 
     * extracts value of ADC at channel_id and calculates corresponding voltage
     * 
     * @param channel_id channel_id corresponding to ADC device
     * @return double measured voltage in mV
     */
    double voltage(byte channel_id);

    /**
     * @brief get value for current measured at ADC
     * 
     * extracts value of ADC at channel_id and calculates corresponding current
     * 
     * @param channel_id channel_id corresponding to ADC device
     * @return double measured voltage in mA
     */
    double current(byte channel_id);

    /**
     * @brief get value of input voltage to MONA Board
     * 
     * extracts value of ADC at connected channel_id for input volate measurement
     * and calculates voltage.
     * 
     * @return double input voltage in mV
     */
    double input_voltage();
    
public:
    Sensor_ADC(const unsigned long samp_time_ms){
        state_ = UNINITIALIZED;
        samp_time_ms_ = samp_time_ms;
    }

    /**
     * @brief call once after declaration to start adc sensors
     */
    void begin();

    /**
     * @brief call once or more often after having called begin method to initialize timing; call after setting system time!
     */
    void init();

    /**
     * @brief 
     * 
     * @param channel_config array of length 4 with bytes 
     * @todo finish description: brief and possible bytes inkl. meaning
     */
    void config_channel(byte channel_config[4]);

    /**
     * @brief get the next schedule for object
     * 
     * depending on Sensor_ADC objects state_ the next schedule time is calculated
     * when in PREPARING mode short time of ?ms to ensure power is switched on
     * when in STANDBY next full period samp_time_ms_ in ms counted from midnight
     * 
     * @param schedule_data 
     * @return struct timeval 
     */
    struct timeval get_next_schedule(void *schedule_data = (void *)NULL);

    /**
     * @brief state_ dependent actions are taken when scheduled
     * 
     * depending on state_ the correct action is called if object is sheduled
     * STANDBY: enable() and state_ is switched to PREPARING
     * PREPARING: take_sample() and disable() and state_ is switched to STANDBY again
     * 
     * @return true when already scheduled and routine was started
     * @return false when not yet scheduled and routine was therefore not started
     */
    bool service_routine();

    /**
     * @brief calculate time weighted average over past timeperiod for since_seconds in seconds
     * @param channel_id of corresponding ADC device
     * @param since_seconds value of seconds into the past to be considered for averaging 
     * @return Sample containing time weighted average and corresponding average timestamp
     */
    Sample get_twa(byte channel_id, int since_seconds);

    /**
     * @brief calculate time weighted average over time interval from to given timestamps
     * @param channel_id of corresponding ADC device
     * @param interval_from timeval of start point for averaging
     * @param interval_to timeval of end point for averaging
     * @return Sample containing time weighted average and corresponding average timestamp
     */
    Sample get_twa(byte channel_id, timeval interval_from, timeval interval_to);

    /**
     * @brief get the Sensor_Value object corresponding to channel_id 
     * @param channel_id of ADC device
     * @return Sensor_Value of corresponding channel_id
     */
    Sensor_Value get_raw_data(byte channel_id);

    /**
     * @brief get value of battery voltage measurement 
     * 
     * extracts value of ADC at connected channel_id for battery volatage measurement
     * and calculates voltage.
     * 
     * @return double battery voltage in mV
     */
    double battery_voltage();

    /**
     * @brief 
     * @todo finish method
     * 
     * @return true 
     * @return false 
     */
    bool ls_permitted(); // or unsigned long (duration of milliseconds over which light sleep is permitted)
};

