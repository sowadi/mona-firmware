
#ifdef ESP_PLATFORM

#include <string.h>
#include <stdlib.h>

#include "sys_event_log.h"
#include "gnss.h"
#include "bg96.h"
#include "mona_time.h"
#include "sys_event_log.h"

/******* public *******/

GNSS::GNSS()
{
    this->success_ = false;
    BG96 bg = BG96::get_instance();
    bg.power_on();
    bg.send_AT_cmd(AT_GPS_TURN_ON, "OK");
    bg.send_AT_cmd("AT+CMEE=2", "OK"); // TODO: Make preproc variable. Activate verbose errors global?
}

GNSS::~GNSS()
{
    BG96 bg = BG96::get_instance();
    bg.send_AT_cmd(AT_GPS_TURN_OFF, "OK");
    bg.power_off();
}

bool GNSS::query_gnss(int timeout)
{
    this->success_ = false;
    BG96 bg = BG96::get_instance();
    ulong start = millis();
    while (millis() - start < timeout)
    {
        #if FAKE_GNSS_RESPONSE
            strcpy(this->response_, "+QGPSLOC: 055900.0,49.88562,8.67008,4.8,129.0,2,0.00,0.0,0.0,140221,02\n\nOK");
            SYS_EVENT_LOGI("Query GNSS was successful.");
            if (this->parse_response())
            {
                SYS_EVENT_LOGI("Parsing GNSS response was successful.");
                this->success_ = true;
                return true;
            }
            SYS_EVENT_LOGE("Parsing GNSS response failed.");
        #endif

        if (bg.send_AT_cmd(AT_GPS_LOC_2, "OK", this->response_))
        {
            SYS_EVENT_LOGI("Query GNSS was successful.");
            if (this->parse_response())
            {
                SYS_EVENT_LOGI("Parsing GNSS response was successful.");
                this->success_ = true;
                return true;
            }
            SYS_EVENT_LOGE("Parsing GNSS response failed.");
            break;
        }
        delay(1000);
    }
    SYS_EVENT_LOGE("Query GNSS failed. Timeout was reached.");
    return false;
}

float GNSS::get_hdop()
{
    if (!this->success_)
    {
        SYS_EVENT_LOGE("Query was not successful. The value in hdop_ is not guaranteed to be correct.");
    }
    return this->hdop_;
}

double GNSS::get_latitude()
{
    if (!this->success_)
    {
        SYS_EVENT_LOGE("Query was not successful. The value in latitude_ is not guaranteed to be correct.");
    }
    return this->latitude_;
}

double GNSS::get_longitude()
{
    if (!this->success_)
    {
        SYS_EVENT_LOGE("Query was not successful. The value in longitude_ is not guaranteed to be correct.");
    }
    return this->longitude_;
}
struct timeval GNSS::get_utc()
{
    if (!this->success_)
    {
        SYS_EVENT_LOGE("Query was not successful. The value in utc_ is not guaranteed to be correct.");
    }
    return this->utc_;
}

/******* private *******/

bool GNSS::parse_response()
{
    SYS_EVENT_LOGD("Start parsing response from GNSS query.");
    // BG96 answers in this form:
    // +QGPSLOC: 201752.0,49.88562,8.67008,4.8,129.0,2,0.00,0.0,0.0,140221,02
    // +QGPSLOC: <utc>,<latitude>,<longitude>,<hdop>,<altitude>,<fix>,<cog>,<spkm>,<spkn>,<date>,<nsat>OK

    if (strstr(this->response_, "+CME ERROR: 516") != NULL)
    {
        return false;
    }

    // response is stored in FLASH. strtok changes the values of the string. but can't change in FLASH. -> strdup creates a changeable copy
    char *s = strdup(this->response_);
    // find the offset at which the data begins
    // in this case: one character after a ' '
    int offset = 0;
    while (s[offset] != ' ') offset++;
    offset++;

    char *utc = s + offset;
    struct tm tm;
    GNSS::parse_to_time(utc, &tm);

    char *token = strtok(s, ",");
    int pos = 0;
    while (token != NULL)
    {
        pos++;
        token = strtok(NULL, ",");
        switch (pos)
        {
        case 1:
            this->latitude_ = atof(token);
            break;
        case 2:
            this->longitude_ = atof(token);
            break;
        case 3:
            this->hdop_ = atof(token);
            break;
        case 9:
            GNSS::parse_to_date(token, &tm);
            break;
        default:
            break;
        }
    }
    free(s);
    this->utc_.tv_sec = mktime(&tm);// MONA_Time::tm_to_timeval(tm);
    return true;
}

int GNSS::parse_to_int(const char hundreds, const char tens, const char ones)
{
  int ihundreds = hundreds - '0';
  int itens = tens - '0';
  int iones = ones - '0';
  return 100 * ihundreds + 10 * itens + iones;
}

void GNSS::parse_to_time(const char *utc, struct tm *res)
{
    res->tm_hour = GNSS::parse_to_int('0', utc[0], utc[1]);
    res->tm_min = GNSS::parse_to_int('0', utc[2], utc[3]);
    res->tm_sec = GNSS::parse_to_int('0', utc[4], utc[5]);
}

void GNSS::parse_to_date(const char *date, struct tm *res)
{
  res->tm_mday = GNSS::parse_to_int('0', date[0], date[1]);
  res->tm_mon = GNSS::parse_to_int('0', date[2], date[3]) - 1;
  int yy = GNSS::parse_to_int('0', date[4], date[5]);
  // is 50 the correct limit?
  if (yy > 50)
  {
    res->tm_year = yy; // necessary?
  }
  else
  {
    // tm_year counts from 1900, but we only have the last two digits of the year
    res->tm_year = yy + 100; 
  }
}

#endif
