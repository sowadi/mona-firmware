
#include <Arduino.h>
#include <driver/rtc_io.h>

#include "sys_event_log.h"
#include "bg96.h"
#include "pin_layout.h"


/******* public *******/

BG96 &BG96::get_instance() {
    static BG96 bg96_instance;
    return bg96_instance;
}

void BG96::power_on() {
    this->_ref_counter++;
    if (this->_ref_counter == 1) {
        rtc_gpio_set_level((gpio_num_t)BG96_EN, HIGH);
        vTaskDelay(5000 / portTICK_RATE_MS);
        bool success;
        while (!(success = this->reset_baudrate())) {
            SYS_EVENT_LOGE("The baudrate for the BG96 could not be set!");
        }
        SYS_EVENT_LOGI("The baudrate for the BG96 has been set to %d", BG96_BAUDRATE);
    }

    this->send_AT_cmd("ATE0", "OK");
}

void BG96::power_off() {
    this->_ref_counter--;
    if (this->_ref_counter == 0) {
        send_AT_cmd(AT_BG96_SHUT_DOWN, "POWERED DOWN");
        vTaskDelay(2000 / portTICK_RATE_MS);
        BG96_Serial.end();
        rtc_gpio_set_level((gpio_num_t)BG96_EN, LOW);
    }
}

bool BG96::send_AT_cmd(const char *cmd, const char *expected) {
    set_AT_cmd(cmd, 30);
    set_AT_expected(expected, 30);
    return at(2000, false, true);
    // return send_AT_cmd(cmd, expected, BG96_DEFAULT_MAX_RESPONSE_TIME_MS, 1, BG96_DEFAULT_MAX_RESPONSE_TIME_MS * 5);
}

bool BG96::send_AT_cmd(const char *cmd, const char *expected, char *response) {
    bool result = send_AT_cmd(cmd, expected);
    memcpy(response, this->at_response_, 256);
    return result;
}

/******* private *******/

BG96::BG96() {
}

void BG96::set_AT_cmd(const char *cmd, size_t l) {
    strlcpy(this->at_command_, cmd, l);
}

void BG96::set_AT_expected(const char *expected, size_t l) {
    strlcpy(this->at_expected_, expected, l);
}

bool BG96::at(ulong timeout, bool check_echo, bool newline) {
    this->clear_buffer();
    size_t bs = newline 
                  ? BG96_Serial.println(this->at_command_)
                  : BG96_Serial.print(this->at_command_);
    SYS_EVENT_LOGI("Printed Bytes to BG96: %d", bs);

    if (check_echo) {
        // if echo mode is enabled, the BG96 response contains the expected command
        memset(this->at_expected_, (char)0, sizeof(this->at_expected_));
        memcpy(this->at_expected_, this->at_command_, sizeof(this->at_command_));
    }

    SYS_EVENT_LOGV("\t--->  %s\t(expecting: %s)", this->at_command_, this->at_expected_);

    // read response
    int result = this->read_response(timeout);

    SYS_EVENT_LOGV("\t<--- %s", this->at_response_);

    // clear members before returning, to ensure the next send_AT_cmd works as expected
    memset(this->at_command_, (char)0, sizeof(this->at_command_));
    memset(this->at_expected_, (char)0, sizeof(this->at_expected_));

    // TODO: a bit scared to touch this... never touch a running system...
    //       But this should be a seperate method `parse_error_code()`.
    // TODO: This is also untested!
    // if (got_error_msg)
    // {
    //     int decimal_factor = 1;
    //     int errorcode = 0;
    //     for (int i = sizeof(this->at_reply_) - 1; i >= 0; --i)
    //     {
    //         if (isdigit(this->at_reply_[i]))
    //         {
    //             errorcode = errorcode + (this->at_reply_[i] - '0') * decimal_factor;
    //             decimal_factor = decimal_factor * 10;
    //         }
    //         else if (this->at_reply_[i] == ':')
    //             break;
    //     }
    //     // setBG96ErrorCode(errorcode); // TODO
    // }

    return BG96_WAS_SUCCESS(result);
}

int BG96::read_response(ulong timeout) {
    ulong start_time = millis();
    memset(this->at_response_, (char)0, sizeof(this->at_response_));
    for (int idx = 0; millis() - start_time < timeout;) {
        delay(1);
        if (BG96_Serial.available()) {
            this->at_response_[idx] = BG96_Serial.read();
            if (strstr(this->at_response_, this->at_expected_) != NULL) {
                return BG96_CORRECT_RESPONSE;
            }
            idx++; // only increment if character has been read
        }
        if (idx >= sizeof(this->at_response_)) {
            return BG96_WRONG_RESPONSE;
        }
    }
    return BG96_WRONG_RESPONSE;
}

void BG96::clear_buffer() {
    while (BG96_Serial.available()) {
        BG96_Serial.read();
    }
}

bool BG96::reset_baudrate() {
    SYS_EVENT_LOGI("Start scanning baudrates for the BG96");
    const int n_baudrates = 17;
    unsigned long baudrates[n_baudrates] = {
        115200, 115200, 115200, 115200, 115200, 115200, // we test 115200 often, because it is very likely, that this is correct
        9600, 300, 1200, 2400, 4800, 19200, 38400, 57600,
        74880, 230400, 250000
    };
    for (int i = 0; i < n_baudrates; i++) {
        BG96_Serial.begin(baudrates[i]);
        SYS_EVENT_LOGD("Trying baudrate: %d", baudrates[i]);
        if (this->send_AT_cmd(AT_SET_BAUD_115200, "OK")) {
            BG96_Serial.begin(BG96_BAUDRATE);
            if (this->send_AT_cmd(AT_BG96_STORE_PARAMETERS, "OK")) { // how often does this fail?
                SYS_EVENT_LOGI("The baudrate of BG96 has been reset to 115200.");
                BG96_Serial.begin(BG96_BAUDRATE);
                return true;
            }
        }
    }
    return false;
}

