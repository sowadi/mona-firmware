#pragma once

#ifdef ESP_PLATFORM

#include <sys/time.h>

/**
 * @brief This class is used to query GNSS data from the BG96.
 * It encapsulates the BG96 class completely. The BG96 is turned on during creation
 * and turned off at destruction of the object.
 * 
 * @note The BG96 is searching for satellites as soon as the GNSS object is constructed.
 * It may be useful to call `query_gnss` with a short timeout, altough it fails, and do other work, before
 * calling `query_gnss` with a longer timeout to acquire the GNSS data.
 */
class GNSS {
public:
    /**
     * @brief Construct a new GNSS object
     * Enables the power delivery and the GNSS functionality of the BG96.
     */
    GNSS();

    /**
     * @brief Destroy the GNSS::GNSS object
     * Disables the GNSS functionality and the power delivery of the BG96.
     */
    ~GNSS();

    /**
     * @brief Query the BG96 for position and time information.
     * 
     * @todo add a minimum acceptable hdop
     * 
     * @param timeout Timeout in milliseconds.
     * @return true If the GNSS data was acquired successfully.
     * @return false If the GNSS data was not acquired successfully.
     */
    bool query_gnss(int timeout = 100000);

    /**
     * @brief Getter method for the hdop
     * 
     * @return float 
     */
    float get_hdop();

    /**
     * @brief Getter method for the latitude
     * 
     * @return double 
     */
    double get_latitude();

    /**
     * @brief Getter method for the longitude
     * 
     * @return double 
     */
    double get_longitude();

    /**
     * @brief Getter method for the utc time
     * 
     * @return double 
     */
    struct timeval get_utc();
private:
    /**
     * @brief the horizontal dilution of precision
     * 
     * @note see https://de.wikipedia.org/wiki/Dilution_of_Precision for more
     */
    float hdop_;

    /**
     * @brief the latitude of the GPS location
     */
    double latitude_;

    /**
     * @brief the longitude of the GPS location
     */
    double longitude_;

    /**
     * @brief the utc time acquired by the BG96
     */
    struct timeval utc_;
    
    /**
     * @brief the response from the BG96
     */
    char response_[256];

    /**
     * @brief holds the success state of the last query
     * 
     */
    bool success_;

    /**
     * @brief Parse the response string after querying the GNSS functionality of the BG96.
     * 
     * @return true if the response was parsed successfully
     * @return false if the response contained an error
     */
    bool parse_response();

    /**
     * @brief Helper function to parse a number from chars to int.
     * 
     * @param hundreds char of the hundreds digit
     * @param tens char of the tens digit
     * @param ones char of the ones digit
     * @return int
     */
    int parse_to_int(const char hundreds, const char tens, const char ones);

    /**
     * @brief Helper function to parse a string to the time part of a `struct tm`.
     * 
     * @param utc Char array of the utc time in the form of 'hhmmss'.
     * @param res Points to the result of the method.
     */
    void parse_to_time(const char *utc, struct tm *res);

    /**
     * @brief Helper function to parse a string to the date part of a `struct tm`.
     * 
     * @param date Char array of the date in the form of 'ddmmyy'.
     * @param res Points to the result of the method.
     */
    void parse_to_date(const char *date, struct tm *res);
};

#endif
