#pragma once

#include <Arduino.h>

#include "mona_scheduler.h"

#define BG96_Serial Serial2

/// The baudrate for the serial communication to the BG96.
#define BG96_BAUDRATE 115200

#define BG96_DEFAULT_MAX_RESPONSE_TIME_MS 300

#define BG96_ERROR_MSG "+CME ERROR:"
#define AT_SET_BAUD_115200 "AT+IPR=115200"
#define AT_BG96_STORE_PARAMETERS "AT&W"
#define AT_BG96_SHUT_DOWN "AT+QPOWD"

#define AT_GPS_STATE "AT+QGPS?"
#define AT_GPS_TURN_ON "AT+QGPS=1"
#define AT_GPS_TURN_OFF "AT+QGPSEND"
#define AT_GPS_LOC_2 "AT+QGPSLOC=2"

#define BG96_CORRECT_RESPONSE 0
#define BG96_WRONG_RESPONSE -1
#define BG96_WAS_SUCCESS(response_code) (response_code == BG96_CORRECT_RESPONSE)


/**
 * @brief This class is responsible for the communication between the µController and the
 * BG96 module.
 */
class BG96 : public MONA_Scheduler
{
public:

    /**
     * @brief Get the instance object of the BG96
     *
     * @return BG96& Singleton object of the BG96
     */
    static BG96 &get_instance();

    /**
     * @brief Enables the power for the BG96 via with the GPIO pin, waits a bit and then calls
     *        reset_baudrate to find the correct baudrate for serial communication.
     *
     */
    void power_on();

    /**
     * @brief Ends serial communcation and disables power for the BG96.
     *
     */
    void power_off();

    /**
     * @brief Sends a AT command to the BG96 and checks if the correct response was received.
     *
     * @param cmd AT command
     * @param expected Response from BG96
     * @return true Expected response was received
     * @return false Expected response was not received
     */
    bool send_AT_cmd(const char *cmd, const char *expected);
    bool send_AT_cmd(const char *cmd, const char *expected, char *response);
    // bool send_AT_cmd(const char *cmd, const char *expected, int single_timeout, int max_tries, int timeout);

    /**
     * @brief Get the reply of the BG96 after an AT command has been sent.
     * @warning This is NOT thread safe. Because this is implemented as a Singelton the
     *          reply could be overwritten from another thread!
     *
     * @return const char* pointer to this->at_reply
     */
    // const char *get_response();

private:

    char at_command_[256];
    char at_expected_[256];
    char at_response_[256];

    static inline int _ref_counter = 0;

    /**
     * Create object to communicate to the bg96 module. There is only one BG96 module on the
     * MONA board, so it probably does not make sense to create multiple instances of this class.
     */
    BG96();

    /**
     * @brief Sets the at command internally so that it can be send later on by at().
     *
     * @param cmd AT command
     * @param l length of the AT command string
     */
    void set_AT_cmd(const char *cmd, size_t l);

    /**
     * @brief Sets the expected response internally so that it can be checked later on by at().
     *
     * @param expected Expected response
     * @param l length of the expected string
     */
    void set_AT_expected(const char *expected, size_t l);

    /**
     * @brief Sends the at command, that has been set earlier with set_AT_cmd, and checks
     *        if the expected response was received, that was set earlier with set_AT_expected.
     *
     * @param timeout timeout in ms
     * @param check_echo checks if at command was echoed back by the BG96 (if the echo mode is turned on)
     * @param newline should be true, if a newline should be added at the end of at command and false,
     *        if a newline should not be added.
     * @return true if expected string was received
     * @return false if expected string was not received
     */
    bool at(ulong timeout, bool check_echo, bool newline);

    /**
     * @brief This is used by at() to read the BG96_Serial buffer, save the received bytes in
     *        at_reply and check if the response was as expected.
     * 
     * @todo handle error codes send by BG96
     * 
     * @param timeout timeout in ms
     * @return int BG96_CORRECT_RESPONSE, if reponse is as expected. BG96_WRONG_RESPONSE, if
     *         response was not as expected.
     */
    int read_response(ulong timeout);

    /**
     * @brief Reads all unread bytes from serial buffer. The buffer is empty after this.
     *
     */
    void clear_buffer();

    /**
     * @brief Checks a set of baudrates until the current rate is found. Then sets the baudrate
     *        to BG96_BAUDRATE.
     * @todo This is probably not the fastest implementation. Maybe it is better to try the more
     *       probable baudrates multiple times, before trying the others. Chances are, that it
     *       failes, because the BG96 is still booting.
     *
     * @return true if baudrate was successfully reset
     * @return false if current baudrate could not be found
     */
    bool reset_baudrate();
};

