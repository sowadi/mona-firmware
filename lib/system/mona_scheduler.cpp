
#include <sys/time.h>

#include "mona_scheduler.h"


void MONA_Scheduler::set_schedule_function(struct timeval func_ptr(void *)) {
    this->schedule_func_ptr_ = func_ptr;
}

struct timeval MONA_Scheduler::get_next_schedule(void *schedule_data) {
    return this->schedule_func_ptr_(schedule_data);
}