#pragma once

#include <sys/time.h>


/**
 * @brief A class that provides the ability to be scheduled to objects of derived classes.
 *        The logic, that calculates the next scheduled point in time, has to be implemented
 *        for each object or class. The `get_next_schedule` method can be either overriden
 *        by the derived class or the default method can be used, to set the schedule function
 *        per object.
 */
class MONA_Scheduler {
private:
    /**
     * @brief This function can be set for each object and gets called by the `get_next_schedule`
     *        method.
     * @note  If the `get_next_schedule` function is overriden by the derived class, this
     *        function may not be called at all.
     *
     * @return This function should return the `struct timeval` time, that hold the time for the
     *         next schedule.
     */
    struct timeval (*schedule_func_ptr_)(void *);
public:
    /**
     * @brief Default constructor. Does nothing interesting.
     *
     */
    MONA_Scheduler() {};

    /**
     * @brief Set the schedule function object
     *
     * @param func_ptr The function, that calculates the next scheduled time. See `schedule_func_ptr_`.
     */
    void set_schedule_function(struct timeval func_ptr(void *));

    /**
     * @brief Get the next scheduled time. This function either executes the `scheduler_func_ptr_`
     *        or is overriden by the derived class.
     *
     * @param schedule_data Data that is necessary to calculate the next scheduled time.
     * @return struct timeval The next scheduled time.
     */
    virtual struct timeval get_next_schedule(void *schedule_data);
};
