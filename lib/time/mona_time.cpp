
#include "mona_time.h"
#include "sys_event_log.h"

MONA_Time::MONA_Time()
{
    rtc.begin();
    set_systime_from_rtc();
}
struct timeval MONA_Time::get_current_time()
{
    struct timeval tv_now;
    //int64_t time_us = (int64_t)tv_now.tv_sec * 1000000L + (int64_t)tv_now.tv_usec;
    if(gettimeofday(&tv_now, NULL))     // gettimeofday returns 0 on success
    {
        SYS_EVENT_LOGW("failed to obtain time");
    }
    if ((tv_now.tv_sec - last_systime_update.tv_sec) > SYSTIME_RTC_UPDATE_INTERVALL)
    {
        // update systime from rtc if time intervall is exceeded
        SYS_EVENT_LOGD("Interval for time update reached.");
        SYS_EVENT_LOGV("Interval difference is: %d", tv_now.tv_sec - last_systime_update.tv_sec);
        tv_now = set_systime_from_rtc();
    }
    return tv_now;
}

void MONA_Time::set_rtc(struct tm new_time)
{
    this->set_rtc(tm_to_DateTime(new_time));
}

void MONA_Time::set_rtc(struct timeval new_time)
{
    this->set_rtc(timeval_to_DateTime(new_time));
}

void MONA_Time::set_rtc(DateTime new_time)
{
    if (!rtc.isrunning())
    {
        rtc.start();
        SYS_EVENT_LOGW("RTC was not running. Attempted to start before setting time.");
    }
    rtc.adjust(new_time);
    last_RTC_time_update = DateTime_to_timeval(new_time);
    char time_format[] = "YY/MM/DD hh:mm:ss";
    SYS_EVENT_LOGI("Real Time Clock has been set to: %s", rtc.now().toString(time_format));
    // when rtc time is set -> the systime is also updated automatically
    set_systime_from_rtc();
}


MONA_Time *MONA_Time::get_instance()
{
    if (!MONA_Time::time_instance)
        MONA_Time::time_instance = new MONA_Time();
    return MONA_Time::time_instance;
}

char *MONA_Time::timeval_to_String(timeval time, char *buffer, const char* format)
{
    char tmbuffer[64];
    time_t nowtime = time.tv_sec;
    struct tm *nowtm = localtime(&nowtime);
    strftime(tmbuffer, sizeof tmbuffer, format, nowtm);
    sprintf(buffer, "(%s.%06ld)", tmbuffer, time.tv_usec);
    return buffer;
}

char *MONA_Time::systime_to_String(char *buffer, const char* format) const
{
    struct timeval tv_now;
    if(gettimeofday(&tv_now, NULL))     // gettimeofday returns 0 on success
    {
        SYS_EVENT_LOGW("failed to obtain time");
    }
    return MONA_Time::timeval_to_String(tv_now, buffer, format);
}

//using Serial debug output instead of syseventlog because of the risk of infinite loop
struct timeval MONA_Time::set_systime_from_rtc()
{
    Serial.println("update system time from RTC");
    if (!rtc.begin())
    {
        Serial.println("RTC not reachable.");
        return timeval{};
    }
    if (rtc.lostPower())
    {
        Serial.println("RTC had a power loss.");
    }

    DateTime dt_rtc = rtc.now();
    if (!rtc.isrunning())
    {
        Serial.println("RTC not running - starting it.");
        rtc.start();
    }
    char time_format[] = "YY/MM/DD hh:mm:ss";
    Serial.printf("External Real Time Clock: %s", rtc.now().toString(time_format));
    timeval epoch = {dt_rtc.unixtime(), 0};
    last_systime_update = epoch;
    settimeofday(&epoch, NULL); //#removed (const timeval *) cast
    return last_systime_update;
}

struct tm MONA_Time::DateTime_to_tm(DateTime dt_time)
{
    time_t unixtime_ = dt_time.unixtime();
    return *(localtime(&unixtime_));
}

DateTime MONA_Time::tm_to_DateTime(struct tm tm_time)
{
    return DateTime(mktime(&tm_time));
}

struct timeval MONA_Time::tm_to_timeval(struct tm tm_time)
{
    timeval tv_time{};
    tv_time.tv_sec = mktime(&tm_time);
    return tv_time;
}
struct tm MONA_Time::timeval_to_tm(struct timeval timeval_time)
{
    return *(localtime(&timeval_time.tv_sec));
}

DateTime MONA_Time::timeval_to_DateTime(struct timeval tv_time)
{
    DateTime dt_time = {tv_time.tv_sec};
    return dt_time;
}

struct timeval MONA_Time::DateTime_to_timeval(DateTime dt_time)
{
    timeval tv_time = {dt_time.unixtime(), 0};
    return tv_time;
}

unsigned long MONA_Time::diff_ms(timeval t1, timeval t2)
{
    if(MONA_Time::later_than(t1,t2)){
        if(t1.tv_usec>t2.tv_usec){
            return ((t1.tv_sec - t2.tv_sec) * 1000) + ((t1.tv_usec - t2.tv_usec)/1000);
        }else{
            return ((t1.tv_sec - t2.tv_sec) * 1000) - ((t2.tv_usec - t1.tv_usec)/1000);
        }
    }else{
        return 0;
    }
}

unsigned long MONA_Time::diff_us(timeval t1, timeval t2)
{
    if(MONA_Time::later_than(t1,t2)){
        if(t1.tv_usec>t2.tv_usec){
            return ((t1.tv_sec - t2.tv_sec) * 1000000) + ((t1.tv_usec - t2.tv_usec));
        }else{
            return ((t1.tv_sec - t2.tv_sec) * 1000000) - ((t2.tv_usec - t1.tv_usec));
        }
    }else{
        return 0;
    }
}

bool MONA_Time::later_than(timeval a, timeval b){
    if(a.tv_sec > b.tv_sec){
        return true;
    }else if(a.tv_sec == b.tv_sec && a.tv_usec > b.tv_usec){
        return true;
    }else{
        return false;
    }
}

MONA_Time *MONA_Time::time_instance = NULL;

// TODO: Add documentation for these two variables!
struct timeval MONA_Time::last_systime_update;
struct timeval MONA_Time::last_RTC_time_update;

