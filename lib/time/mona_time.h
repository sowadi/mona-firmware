#pragma once

#include "RTClib.h"
#include <SD_MMC.h>
#include <sys/time.h>

/* Constants */
#define SECONDS_PER_HOUR 3600
/**************************************************************************/

/**
 * @brief Encapsulates time management using the system time and rtc time under the hood.
 * rtc time should be set via set_rtc() using the gps time from the bg96 module to ensure
 * accuracy
 *
 * Implemented as Singleton ->
 * might not be thread safe. Use mutex when accessing instance from
 * multiple threads at the same time.
 *
 *
 * Use method get_instance() to access a MONA_Time instance instead of using the
 * constructor manually because only one instance of this class must exist at any time
 * and this class is therefore realized as a Singleton.
 *
 * uses time in the form of timeval and tm objects
 * see
 * https://renenyffenegger.ch/notes/development/languages/C-C-plus-plus/C/libc/structs/timeval
 * and
 * https://www.cplusplus.com/reference/ctime/tm/
 */

class MONA_Time
{
public:
    /**
     * @brief get the current time and update the systime from rtc if the update intervall
     * is exceeded
     * @return the current system time
     */
    struct timeval get_current_time();

    /**
     * @brief set the time of the Real Time Clock (RTC)
     *
     * @note the systime is updated to the new rtc time too
     *
     * @param time - the time the rtc is updated to
     */
    void set_rtc(struct tm time);
    void set_rtc(struct timeval time);
    void set_rtc(DateTime time);

    /**
     * @brief Get the Singleton Instance of MONA_Time
     *
     * @warning protect calls with mutex to secure thread safety if needed
     */
    static MONA_Time *get_instance();

    /**
     * @brief converts a timeval timestamp into a string
     *
     * @param time the timeval to be converted
     * @param[in,out] buffer the char array the resulting string is written to. "char buf[64];" should work in most cases"
     * @param format the format which is used to print out the time. see strftime() documentation for more details.microseconds of the timeval are appended with 6 digits in any case
     * @return char* the resulting time string
     */
    static char *timeval_to_String(timeval time, char *buffer, const char *format = "%Y-%m-%d %H:%M:%S");

    /**
     * @brief Writes the system time as a string in a user-defined format.
     *
     * __Example__: The following code results in the printing of the timestamp to
     * Serial in the format: `2022-04-21 17:10:19.098643`
     *  char buf[64];
     *  const char *format = "%Y-%m-%d %H:%M:%S";
     *  Serial.println(time_->systime_to_String(buf, format));
     *  whereas time_ is an instance of the MONA_Time
     *
     * @param[in,out] buffer Array of `char` the resulting  
     * string is written to. "char buf[64];" should work in
     * most cases"
     *
     * @param format the format which is used to print out the time. see strftime() documentation for more details. The microseconds of the timeval are appended with 6 digits in any case
     *
     * @return A pointer to the provided buffer.
     *
     */
    char *systime_to_String(char *buffer, const char* format = "%Y-%m-%d %H:%M:%S") const;

    /**
     * @brief converts timestamp from a DateTime to a tm object
     *
     * @param dt_time the timestamp as DateTime type
     * @return struct tm the converted timestamp
     */
    static struct tm DateTime_to_tm(DateTime dt_time);

    /**
     * @brief converts timestamp from a tm to a DateTime object
     *
     * @param tm_time the timestamp as DateTime type
     * @return DateTime the converted timestamp
     */
    static DateTime tm_to_DateTime(struct tm tm_time);

    /**
     * @brief converts timestamp from a struct tm to a struct timeval object
     *
     * @param tm_time the timestamp as struct tm type
     * @return struct timeval the converted timestamp
     */
    static struct timeval tm_to_timeval(struct tm tm_time);

    /**
     * @brief converts timestamp from a struct timeval to a struct tm object
     *
     *
     * @warning losing precision - microseconds get lost through conversion
     * @param tv_time the timestamp as timeval type
     * @return struct tm the converted timestamp
     */
    static struct tm timeval_to_tm(struct timeval tv_time);

    /**
     * @brief converts timestamp from a struct timeval to a DateTime object
     *
     *
     * @warning losing precision - microseconds get lost through conversion
     * @param tv_time the timestamp as timeval type
     * @return DateTime the converted timestamp
     */
    static DateTime timeval_to_DateTime(struct timeval tv_time);

    /**
     * @brief converts timestamp from a DateTime to a struct timeval object
     *
     * @param dt_time the timestamp as DateTime type
     * @return struct timeval the converted timestamp
     */
    static struct timeval DateTime_to_timeval(DateTime dt_time);

    /**
     * @brief returns t1 - t2 in milli seconds if t1 > t2, else zero
     */
    static unsigned long diff_ms(timeval t1, timeval t2);

    /**
     * @brief returns t1 - t2 in micro seconds if t1 > t2, else zero
     */
    static unsigned long diff_us(timeval t1, timeval t2);

    /**
     * @brief returns true if a is later then b, otherwise false
     */
    static bool later_than(timeval a, timeval b);

private:
    /**
     * @brief holds the singleton instance of the MONA_Time class
     *
     */
    static MONA_Time *time_instance;

    /**
     * @brief the real time clock
     *
     */
    RTC_PCF8563 rtc;

    /**
     * @brief timestamp of last update of systime from rtc time
     *
     */
    static struct timeval last_systime_update;

    /**
     * @brief timestamp of last update of rtc time via set_rtc()
     *
     */
    static struct timeval last_RTC_time_update;

    /**
     * @brief intervall which determines how often the systime is updated from the rtc
     *
     */
    static const u_int SYSTIME_RTC_UPDATE_INTERVALL = SECONDS_PER_HOUR;

    /**
     * @brief Construct a new mona time object
     * Private to prevent creation of multiple instances
     */
    MONA_Time();

    /**
     * @brief update the systime using the current rtc time
     *
     * @return the current time from the rtc
     */
    struct timeval set_systime_from_rtc();
};