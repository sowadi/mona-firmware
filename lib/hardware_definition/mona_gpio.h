
/**
 * @brief Lock a gpio pin, to ensure it does not change.
 *        This can be usefull during sleep.
 * 
 * @param num gpio pin number
 * @param enable true enables locking, false disables locking
 */
void pin_lock(int num, bool enable);

/**
 * @brief Locks all lockable pins (on the esp32), see ::pin_lock for
 *        more information on locking. BG96
 * 
 * @param enable true enables locking, false disables locking
 */
void pin_lock_all(bool enable);

/**
 * @brief Initialize the necessary gpio pins and set their io direction and default level.
 *        All pin locks are disabled (see #pin_lock_all).
 * 
 */
void init_gpios(void);
