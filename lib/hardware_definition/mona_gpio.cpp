
#ifdef ESP_PLATFORM

#include <Arduino.h>
#include <driver/rtc_io.h>

#include "SD_MMC.h"

#include "pin_layout.h"
#include "mona_gpio.h"

void pin_lock(int num, bool enable){
  if(enable){
    rtc_gpio_hold_en((gpio_num_t)num);
  }
  else{
    rtc_gpio_hold_dis((gpio_num_t)num);
  }
}

void pin_lock_all(bool enable){
  // dont use aliases, because only the ones listed here can be locked.
  pin_lock(0,enable);                  
  pin_lock(2,enable);
  pin_lock(4,enable);
  pin_lock(12,enable);
  pin_lock(13,enable);
  //pin_lock(14,enable);  // do not lock pin 14
  pin_lock(15,enable);
  pin_lock(25,enable);
  pin_lock(26,enable);
  pin_lock(27,enable);
}

void init_gpios(void) {
  // initialize GPIOs as RTC GPIOs. Only these pins have the ability to hold their state during sleep
  rtc_gpio_init((gpio_num_t)T_EN);
  rtc_gpio_init((gpio_num_t)FLUSH_EN);
  rtc_gpio_init((gpio_num_t)FLUSH);
  rtc_gpio_init((gpio_num_t)LIGHT_EN);
  rtc_gpio_init((gpio_num_t)BG96_EN);
  rtc_gpio_init((gpio_num_t)RXB6_EN);
  rtc_gpio_init((gpio_num_t)RAIN_EN);
  rtc_gpio_init((gpio_num_t)SD_EN);
  rtc_gpio_init((gpio_num_t)BLE_TRIGGER);
  // configure RTC GPIOs as input or output
  rtc_gpio_set_direction((gpio_num_t)T_EN, RTC_GPIO_MODE_OUTPUT_ONLY);
  rtc_gpio_set_level((gpio_num_t)T_EN, HIGH);   // HIGH = thermometers not enabled (saves power)
  rtc_gpio_set_direction((gpio_num_t)FLUSH_EN, RTC_GPIO_MODE_OUTPUT_ONLY); 
  rtc_gpio_set_direction((gpio_num_t)FLUSH, RTC_GPIO_MODE_INPUT_ONLY);
  rtc_gpio_set_direction((gpio_num_t)2, RTC_GPIO_MODE_INPUT_ONLY);
  rtc_gpio_set_direction((gpio_num_t)15, RTC_GPIO_MODE_INPUT_ONLY);
  rtc_gpio_set_direction((gpio_num_t)LIGHT_EN, RTC_GPIO_MODE_OUTPUT_ONLY);
  rtc_gpio_set_direction((gpio_num_t)BG96_EN, RTC_GPIO_MODE_OUTPUT_ONLY);
  rtc_gpio_set_direction((gpio_num_t)RXB6_EN, RTC_GPIO_MODE_OUTPUT_ONLY); 
  rtc_gpio_set_direction((gpio_num_t)RAIN_EN, RTC_GPIO_MODE_OUTPUT_ONLY);
  rtc_gpio_set_direction((gpio_num_t)SD_EN, RTC_GPIO_MODE_OUTPUT_ONLY);
  rtc_gpio_set_level((gpio_num_t)SD_EN, LOW);
  rtc_gpio_set_direction((gpio_num_t)BLE_TRIGGER,RTC_GPIO_MODE_INPUT_ONLY);
  // disable hold on all RTC-GPIOs (might have been active from sleep)
  pin_lock_all(false);
  // configure non-RTC-GPIOs. THESE PINS WILL FLOAT DURING SLEEP! (some pins can not be configured as RTC-GPIOs)
  pinMode(BG96_RX,INPUT);
  pinMode(BG96_TX,INPUT);
  pinMode(RAIN,INPUT);
  pinMode(STATUSLIGHT,OUTPUT);
  // call SD_MMC.begin() once to initialize pins of SD-Card
  rtc_gpio_set_level((gpio_num_t)SD_EN, HIGH);  // switch on power
  delay(10);                                    // wait for a sufficient amount of time
  SD_MMC.begin("/sdcard", true);
  SD_MMC.end();
  delay(10);
  rtc_gpio_set_level((gpio_num_t)SD_EN, LOW);
}

#endif