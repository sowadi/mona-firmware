#pragma once

#define DEVICE_NAME "MONA-01" // TODO: dynamically check for device

#define T_EN        0
#define T_DIS       0         // DIS = disable is more descriptive for the functionality (active low)
#define BG96_EN     4
#define STATUSLIGHT 5
#define SD_EN       12
#define RAIN_EN     13
#define BG96_TX     16
#define BG96_RX     17
//      SCK         18
//      MISO        19
#define SDA_A       21
#define SCL_A       22
//      MOSI        23
#define FLUSH_EN    25
#define LIGHT_EN    26
#define RXB6_EN     27
#define RAIN        34
#define RXB6_DATA   35
#define BLE_TRIGGER 36
#define FLUSH       39

