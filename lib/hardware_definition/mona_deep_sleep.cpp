#include <Arduino.h>
#include <RTClib.h>
#include <driver/rtc_io.h>

#include "mona_light_sleep.h"
#include "mona_deep_sleep.h"
#include "pin_layout.h"
#include "mona_gpio.h"
#include "mona_time.h"
#include "sys_event_log.h"


MONA_Deep_Sleep::MONA_Deep_Sleep(){
    deep_sleep_start_.hour = 20;
    deep_sleep_start_.minute = 0;
    deep_sleep_stop_.hour = 6;
    deep_sleep_stop_.minute = 0;
}

MONA_Deep_Sleep *MONA_Deep_Sleep::get_instance()
{
    if (!MONA_Deep_Sleep::deep_sleep_instance)
    {
        MONA_Deep_Sleep::deep_sleep_instance = new MONA_Deep_Sleep();
    }
    return MONA_Deep_Sleep::deep_sleep_instance;
}

void MONA_Deep_Sleep::update_tv_start_and_stop(){
    MONA_Time* time_instance = MONA_Time::get_instance();
    struct timeval tv_current = time_instance->get_current_time();
    struct tm tm_current = MONA_Time::timeval_to_tm(tv_current);
    
    struct tm tm_deep_sleep_stop = tm_current;
    tm_deep_sleep_stop.tm_hour = deep_sleep_stop_.hour;
    tm_deep_sleep_stop.tm_min = deep_sleep_stop_.minute;
    tm_deep_sleep_stop.tm_sec = 0;
    this->tv_deep_sleep_stop_ = MONA_Time::tm_to_timeval(tm_deep_sleep_stop);
    
    if(MONA_Time::later_than(tv_current, this->tv_deep_sleep_stop_)){
        this->tv_deep_sleep_stop_.tv_sec = this->tv_deep_sleep_stop_.tv_sec + 24*60*60;
    }

    struct tm tm_deep_sleep_start = tm_current;
    tm_deep_sleep_start.tm_hour = deep_sleep_start_.hour;
    tm_deep_sleep_start.tm_min = deep_sleep_start_.minute;
    tm_deep_sleep_start.tm_sec = 0;
    this->tv_deep_sleep_start_ = MONA_Time::tm_to_timeval(tm_deep_sleep_start);
    
    if(MONA_Time::later_than(tv_current, this->tv_deep_sleep_start_)){
        this->tv_deep_sleep_start_.tv_sec = this->tv_deep_sleep_start_.tv_sec + 24*60*60;
    }

}

bool MONA_Deep_Sleep::prepare(){
    rtc_gpio_set_level((gpio_num_t)RXB6_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)BG96_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)SD_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)T_DIS, HIGH);
    digitalWrite(RAIN_EN,LOW); //for some reason setting the RTC-GPIO does not work, but this does
    pin_lock_all(true);
    return true;
}

bool MONA_Deep_Sleep::permission(){
    this->update_tv_start_and_stop();
    //MONA_Time* time_instance = MONA_Time::get_instance();
    if(MONA_Time::later_than(this->tv_deep_sleep_stop_,this->tv_deep_sleep_start_)){
        return false;
    }else{
        MONA_Light_Sleep* light_sleep_instance = MONA_Light_Sleep::get_instance();
        return light_sleep_instance->permission();
    }
}

void MONA_Deep_Sleep::start()
{
    //this->update_tv_start_and_stop();
    if(this->permission()){
        MONA_Time* time_instance = MONA_Time::get_instance();
        struct timeval tv_current = time_instance->get_current_time();
        unsigned long time_to_sleep_ms = MONA_Time::diff_ms(this->tv_deep_sleep_stop_, tv_current);
        SYS_EVENT_LOGI("Deep sleeping for %d ms.",time_to_sleep_ms);
        Serial.flush();
        esp_sleep_enable_timer_wakeup(time_to_sleep_ms * 1000);
        esp_deep_sleep_start();
        // esp32 will be reset; no code after this point will be executed
    }
    
}

void MONA_Deep_Sleep::set_deep_sleep_interval(byte start_hour, byte start_minute, byte stop_hour, byte stop_minute){
    deep_sleep_start_.hour = start_hour;
    deep_sleep_start_.minute = start_minute;
    deep_sleep_stop_.hour = stop_hour;
    deep_sleep_stop_.minute = stop_minute;
}

MONA_Deep_Sleep *MONA_Deep_Sleep::deep_sleep_instance = NULL;