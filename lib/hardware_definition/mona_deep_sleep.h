#pragma once

/**
 * @brief Basic tuple containing hour and minute.
 */
typedef struct {
    byte hour;
    byte minute;
} Daytime;

/**
 * @brief Manage permission and process of entering deep sleep mode.
 * 
 */
class MONA_Deep_Sleep
{
private:
    /**
     * @brief Initializes Mona to deep sleep between 20:00:00 and 06:00:00
     */
    MONA_Deep_Sleep();

    struct timeval tv_deep_sleep_start_;
    struct timeval tv_deep_sleep_stop_;
    Daytime deep_sleep_start_;
    Daytime deep_sleep_stop_;

    /**
     * @brief holds the singleton instance of the Deep_Sleep class
     *
     */
    static MONA_Deep_Sleep *deep_sleep_instance;

    /**
     * @brief Prepares the hardware for a power efficient deep sleep.
     */
    bool prepare();

public:

    /**
     * @brief Get the Singleton Instance of MONA_Deep_Sleep
     *
     * @warning protect calls with mutex to secure thread safety if needed
     */
    static MONA_Deep_Sleep *get_instance();

     /**
     * @brief Updates tv_deep_sleep_start_ and tv_deep_sleep_stop_ to the current future values
     */
    void update_tv_start_and_stop();
    
    /**
     * @brief Checks permission of ESP32 to go to deep sleep.
     */
    bool permission();

    /**
     * @brief Puts the ESP32 to deep sleep
     * this should correspond to < 80uA of current consumption
     * esp32 will be reset in the end
     */
    void start();

    /**
     * @brief Set the interval for monas deep sleep to override default value
    */
   void set_deep_sleep_interval(byte start_hour, byte start_minute, byte stop_hour, byte stop_minute);
    
};