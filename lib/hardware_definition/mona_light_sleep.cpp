#include <Arduino.h>
#include <RTClib.h>
#include <driver/rtc_io.h>

#include "mona_light_sleep.h"
#include "pin_layout.h"
#include "mona_gpio.h"
#include "mona_time.h"
#include "sys_event_log.h"


MONA_Light_Sleep::MONA_Light_Sleep(){
    veto_counter_ = 0;
}

MONA_Light_Sleep *MONA_Light_Sleep::get_instance()
{
    if (!MONA_Light_Sleep::light_sleep_instance)
    {
        MONA_Light_Sleep::light_sleep_instance = new MONA_Light_Sleep();
    }
    return MONA_Light_Sleep::light_sleep_instance;
}

bool MONA_Light_Sleep::increment_veto(){
    this->veto_counter_ += 1;
    SYS_EVENT_LOGV("Incremented Veto");
    if (this->veto_counter_ < 1){
        // TODO: possibly add some error msg
        return false;
    }else{
        return true;
    }
}

bool MONA_Light_Sleep::decrement_veto(){
    this->veto_counter_ -= 1;
    SYS_EVENT_LOGV("Decremented Veto");
    if (this->veto_counter_ < 0){
        // TODO: possibly add some error msg
        return false;
    }else{
        return true;
    }
}

bool MONA_Light_Sleep::permission(){
    SYS_EVENT_LOGD("Veto Counter = %d",veto_counter_);
    return this->veto_counter_ <= 0;
}

bool MONA_Light_Sleep::prepare(){
    if (!this->permission()){
        // TODO: add some error msg
        return false;
    }
    rtc_gpio_set_level((gpio_num_t)RXB6_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)LIGHT_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)BG96_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)SD_EN, LOW);
    rtc_gpio_set_level((gpio_num_t)T_DIS, HIGH);
    digitalWrite(RAIN_EN,LOW); //for some reason setting the RTC-GPIO does not work, but this does
    pin_lock_all(true);
    return true;
}

void MONA_Light_Sleep::start()
{
    if (this->tv_wake_up_time_.tv_sec == 0 && this->tv_wake_up_time_.tv_usec == 0)
    {
        SYS_EVENT_LOGV("Sleeping for 0 seconds is not sleepig.");
        return;
    }
    if (!this->permission()){
        SYS_EVENT_LOGV("Sleeping not permitted.");
        // TODO: add some error msg
        return;
    }
    MONA_Time* time_instance = MONA_Time::get_instance();
    struct timeval tv_current = time_instance->get_current_time();
    unsigned long time_to_sleep_ms = MONA_Time::diff_ms(this->tv_wake_up_time_, tv_current);

    //char temp_buf[64];
    //MONA_Time::timeval_to_String(this->tv_wake_up_time_, temp_buf);

    //char temp_buf2[64];
    //MONA_Time::timeval_to_String(tv_current, temp_buf2);
    
    SYS_EVENT_LOGI("Light sleeping for %d ms.",time_to_sleep_ms);
    this->prepare();
    esp_sleep_enable_timer_wakeup(time_to_sleep_ms*1000);
    Serial.flush();
    esp_light_sleep_start();
    this->reset_wake_up_time();
    pin_lock_all(false);
}

void MONA_Light_Sleep::update_wake_up_time(struct timeval tv_next)
{
    if (this->tv_wake_up_time_.tv_sec == 0 && this->tv_wake_up_time_.tv_usec == 0)
    {
        this->tv_wake_up_time_ = tv_next;
    }

    if (MONA_Time::later_than(this->tv_wake_up_time_, tv_next))
    {
        this->tv_wake_up_time_ = tv_next;
    }
}

void MONA_Light_Sleep::reset_wake_up_time()
{
    MONA_Time* time_instance = MONA_Time::get_instance();
    this->tv_wake_up_time_ = time_instance->get_current_time();
    tv_wake_up_time_.tv_sec += 60;
}


MONA_Light_Sleep *MONA_Light_Sleep::light_sleep_instance = NULL;
struct timeval MONA_Light_Sleep::tv_wake_up_time_ = (struct timeval){0, 0};