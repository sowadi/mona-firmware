#pragma once

/**
 * @brief Manage permission and process of entering light sleep mode.
 * 
 */
class MONA_Light_Sleep
{
private:
    MONA_Light_Sleep();

    int veto_counter_;

    /**
     * @brief Holds an absolute time that describes the next time Mona has to be awake
     * @todo is it really sufficient to only store the closest tv_wake_up_time_? Dont we have to store all of them? Can we maybe get around this by using callback functions?
     * {0, 0} means no time set. Don't go to sleep. (gets initialized as {0,0})
     */
    static struct timeval tv_wake_up_time_;

    /**
     * @brief holds the singleton instance of the Light_Sleep class
     *
     */
    static MONA_Light_Sleep *light_sleep_instance;

    /**
     * @brief Prepares the hardware for a power efficient light sleep.
     */
    bool prepare();

    /**
     * @brief Sets default wake up time after light sleep.
     * Light sleep is intended to be never longer than 60 seconds. For this reason, after waking up, the next wake up is scheduled for in 60 seconds.
     */
    void reset_wake_up_time();


public:

    /**
     * @brief Get the Singleton Instance of MONA_Light_Sleep
     *
     * @warning protect calls with mutex to secure thread safety if needed
     */
    static MONA_Light_Sleep *get_instance();

    /**
     * @brief Increments the veto counter for entering light sleep.
     */
    bool increment_veto();

    /**
     * @brief Decrements the veto counter for entering light sleep.
     */
    bool decrement_veto();

    /**
     * @brief Checks for any veto against light sleeping.
     * 
     * @return true if light sleep is permitted
     */
    bool permission();

    /**
     * @brief Puts the ESP32 to light sleep (clock gating)
     * should correspond to < 1.9mA of current consumption
     */
    void start();

    /**
     * @brief Updates the wake up time if the given point in time is before the stored wake up time
     */
    void update_wake_up_time(struct timeval tv_next);

    
};