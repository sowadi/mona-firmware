#pragma once

#include <sys/time.h>

/**
 * @brief Size of one log row of the CONTI_Data class in bytes.
 */
#define CONTI_ROW_LEN_BYTES 38

/**
 * @brief Size of one log row of the SYLOG_Data class in bytes.
 */
#define SYLOG_ROW_LEN_BYTES 6

/**
 * @brief not necessary?
 */
#define LOG_MAX_ROW_LEN CONTI_ROW_LEN_BYTES

/**
 * @brief Max size of a filename of a log file in bytes.
 */
#define MONA_LOG_FILENAME_LEN 30

/**
 * @brief Postfix for the SYLOG_Data log filename.
 */
#define SYLOG_FILE_POSTFIX "SYLOG"

/**
 * @brief Postfix for the CONTI_Data log filename.
 */
#define CONTI_FILE_POSTFIX "CONTI"

#define CONTI_CSV_COLUMN_NAMES "TIMESTAMP;V_BAT;T0;T1;T2;T3\n"
#define CONTI_CSV_COLUMN_UNITS "HH:MM:SS;V;degC;degC;degC;degC\n"

/**
 * @brief Enum for all types of logfiles that are available
 */
enum Mona_Log_File_Type {
    CONTI,
    SYLOG,
};


/**
 * @brief Baseclass for data that can be logged.
 * To implement a loggable class, derive from `Log_Data` and add all relevant member
 * variables.
 */
class Log_Data {
private:
protected:
    /**
     * @brief Every data log has a corresponding time.
     */
    struct timeval timestamp_;

    /**
     * @brief Construct a new Log_Data object with the corresponding time.
     *
     * @param timestamp `timestamp_`
     */
    Log_Data(struct timeval timestamp) : timestamp_(timestamp) {}

    /**
     * @brief 
     * 
     */
    void to_date_string(char *date, struct timeval tv);
public:
    /**
     * @brief Create the string representation of the log.
     *
     * @param out Holds the string representation
     */
    virtual void to_string(char *out) = 0;

    /**
     * @brief Get the length of the log row (string representation) in bytes.
     *
     * @return int
     */
    virtual int get_row_len_bytes() = 0;

    /**
     * @brief Get the file type of the log object.
     *
     * @return Mona_Log_File_Type
     */
    virtual Mona_Log_File_Type get_file_type() = 0;

    /**
     * @brief Save the log object to the corresponding file.
     *
     * @return true success
     * @return false failure
     */
    virtual bool log() = 0;

    /**
     * @brief Get the path to the file where the current log entry should be saved.
     *
     * @param path Output, that holds the path string
     * @param dt Corresponding time
     * @return int number of bytes written
     */
    virtual int get_path(char *path, struct timeval tv) = 0;

    /**
     * @brief Delete the complete log file that corresponds to the given time.
     *
     * @warning Deletes the complete log file. NOT just the log entry!
     *
     * @param dt Time, which is used to find the corresponding file.
     * @return true success
     * @return false failure
     */
    virtual bool delete_logfile(struct timeval tv) = 0;
};


 /**
  * @brief For more documentation see the `Log_Data` parent class.
  */
class CONTI_Data : public Log_Data {
private:
    double battery_voltage_;
    double temperature_T0_;
    double temperature_T1_;
    double temperature_T2_;
    double temperature_T3_;

public:
    static const int row_len_bytes = CONTI_ROW_LEN_BYTES;

    CONTI_Data(struct timeval timestamp, double battery_voltage, double temperature_T0, double temperature_T1, double temperature_T2, double temperature_T3)
            : Log_Data(timestamp), battery_voltage_(battery_voltage), temperature_T0_(temperature_T0), temperature_T1_(temperature_T1), temperature_T2_(temperature_T2), temperature_T3_(temperature_T3) {}

    void to_string(char *out);
    int get_row_len_bytes() {return row_len_bytes;}
    Mona_Log_File_Type get_file_type() { return CONTI; };
    bool log();
    int get_path(char *path, struct timeval tv);
    bool delete_logfile(struct timeval tv);
};


 /**
  * @brief For more documentation see the `Log_Data` parent class.
  */
class SYLOG_Data : public Log_Data {
private:
    int error_code_;

public:
    static const int row_len_bytes = SYLOG_ROW_LEN_BYTES;

    SYLOG_Data(struct timeval timestamp, int error_code) : Log_Data(timestamp), error_code_(error_code) {}
    
    void to_string(char *out);
    int get_row_len_bytes() {return row_len_bytes;}
    Mona_Log_File_Type get_file_type() { return SYLOG; };
    bool log();
    int get_path(char *path, struct timeval tv);
    bool delete_logfile(struct timeval tv);
};
