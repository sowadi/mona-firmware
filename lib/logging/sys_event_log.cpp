#include "sys_event_log.h"
#include "mona_time.h"
#include <stdarg.h>

#ifndef FILE_DEBUG_LEVEL
#define FILE_DEBUG_LEVEL 2
#endif
#ifndef SERIAL_DEBUG_LEVEL
#define SERIAL_DEBUG_LEVEL 3
#endif
#ifndef LOG_TIMESTAMPS
#define LOG_TIMESTAMPS false
#endif

size_t buffer_size = 1000; //set to 2 or 4 kb later
//interval which determines how often the buffer is flushed to sd card even though it might not yet be full
static const u_int SD_CARD_FLUSH_INTERVAL = 10;
struct timeval last_sd_card_flush_tv;
char *logs_buffer;

void init_sys_event_log(){
    Serial.begin(115200);
    logs_buffer = (char *)malloc(buffer_size * sizeof(char));
    logs_buffer[0] = '\0';
    struct timeval last_sd_card_flush_tv = MONA_Time::get_instance()->get_current_time();
} 

void sys_event_log(LOGLVL_ENUM loglvl, const char *file, int line, const char *function, const char *msg_format, ...)
{
    if (FILE_DEBUG_LEVEL < loglvl && SERIAL_DEBUG_LEVEL < loglvl)
    {
        return;
    }

    //build logmessage-string with passed arguments
    va_list args;
    char msg_buffer[256];
    va_start(args, msg_format);
    vsnprintf(msg_buffer, sizeof(msg_buffer), msg_format, args);
    va_end(args);

    //build final log-string with additional info
    char log_buffer[512];
#if (LOG_TIMESTAMPS)
    time_t cur_time = time(NULL);
    struct tm* tmp = localtime(&cur_time);
    char time_string[50];
    strftime(time_string, sizeof(time_string), "%d.%m. - %H:%M:%S", tmp);
    snprintf(log_buffer, sizeof(log_buffer), "[%s][%s][%s:%u] %s(): %s\n", LOGLVL_STRING[loglvl], time_string, pathToFileName(file), line, function, msg_buffer);
#else
    snprintf(log_buffer, sizeof(log_buffer), "[%s][%s:%u] %s(): %s\n", LOGLVL_STRING[loglvl], pathToFileName(file), line, function, msg_buffer);
#endif
    if (FILE_DEBUG_LEVEL >= loglvl)
    {
        // Copy log message into the buffer
        strncat(logs_buffer, log_buffer, buffer_size - strlen(logs_buffer));
        if (buffer_size - strlen(logs_buffer) < 100)
        {
            flush_buffer();
        }
        struct timeval tv_now = MONA_Time::get_instance()->get_current_time();
        if ((tv_now.tv_sec - last_sd_card_flush_tv.tv_sec) > SD_CARD_FLUSH_INTERVAL)
        {
            flush_buffer();
            last_sd_card_flush_tv = tv_now;
        }
    }
    if (SERIAL_DEBUG_LEVEL >= loglvl)
    {
        Serial.printf(log_buffer);
    }
}

void flush_buffer()
{
    //actually write to sd card
    MONA_File log_file{};
    log_file.append(LOG_FILE_PATH, logs_buffer);
    //reset the buffer
    memset(logs_buffer, 0, buffer_size);
    logs_buffer[0] = '\0';

    //debugging
    Serial.println("flushed buffer");
}