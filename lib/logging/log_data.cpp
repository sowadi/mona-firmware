
#include <cstdio>
#include <sys/time.h>

#include "sys_event_log.h"
#include "log_data.h"
#include "mona_file.h"
#include "mona_time.h"
#include "pin_layout.h"
#include "mona_time.h"
#include "RTClib.h"

#ifdef ESP_PLATFORM
#define PATH_BEGIN "/"
#else
#define PATH_BEGIN ""
#endif


void Log_Data::to_date_string(char *date, struct timeval tv)
{
    struct tm dt = MONA_Time::timeval_to_tm(tv);
    sprintf(date, "%04d-%02d-%02d", dt.tm_year + 1900, dt.tm_mon + 1, dt.tm_mday);
}

void CONTI_Data::to_string(char *out) {
    struct tm tm = MONA_Time::timeval_to_tm(this->timestamp_);
    sprintf(out, "%02d:%02d:%02d;%1.3f;%3.1f;%3.1f;%3.1f;%3.1f\n", 
        tm.tm_hour, tm.tm_min, tm.tm_sec, battery_voltage_/1000, temperature_T0_, temperature_T1_, temperature_T2_, temperature_T3_);
}

bool CONTI_Data::log()
{
    char row[CONTI_ROW_LEN_BYTES] = {0};
    this->to_string(row);
    char path[MONA_LOG_FILENAME_LEN] = {0};
    MONA_Time *mt = MONA_Time::get_instance();
    struct timeval tv = mt->get_current_time();
    this->get_path(path, tv);
    SYS_EVENT_LOGV("Data log file path: %s", path);
    MONA_File file;
    if (!SD_MMC.exists(path)) {
        file.append(path, CONTI_CSV_COLUMN_NAMES);
        file.append(path, CONTI_CSV_COLUMN_UNITS);
    }
    return file.append(path, row);
}

int CONTI_Data::get_path(char *path, struct timeval tv)
{
    char date[11] = {0};
    this->to_date_string(date, tv);
    snprintf(path, sizeof(path), PATH_BEGIN DEVICE_NAME "_%s_" CONTI_FILE_POSTFIX ".csv", date);
    return snprintf(path, MONA_LOG_FILENAME_LEN, PATH_BEGIN DEVICE_NAME "_%s_" CONTI_FILE_POSTFIX ".csv", date);
}

bool CONTI_Data::delete_logfile(struct timeval tv)
{
    char path[MONA_LOG_FILENAME_LEN] = {0};
    this->get_path(path, tv);
    MONA_File file;
    return file.delete_file(path);
}

void SYLOG_Data::to_string(char *out) {
    sprintf(out, "%1d,%2d\n", timestamp_.tv_sec, error_code_);
}

bool SYLOG_Data::log()
{
    char row[CONTI_ROW_LEN_BYTES] = {0};
    this->to_string(row);
    char path[MONA_LOG_FILENAME_LEN] = {0};
    MONA_Time *mt = MONA_Time::get_instance();
    struct timeval tv = mt->get_current_time();
    this->get_path(path, tv);
    MONA_File file;
    return file.append(path, row);
}

int SYLOG_Data::get_path(char *path, struct timeval tv)
{
    char date[11] = {0};
    this->to_date_string(date, tv);
    return snprintf(path, sizeof(path), PATH_BEGIN DEVICE_NAME "_%s_" SYLOG_FILE_POSTFIX ".csv", date);
}

bool SYLOG_Data::delete_logfile(struct timeval tv)
{
    char path[MONA_LOG_FILENAME_LEN] = {0};
    this->get_path(path, tv);
    MONA_File file;
    return file.delete_file(path);
}
