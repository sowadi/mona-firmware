
#include <SD_MMC.h>
#include <FS.h>
#include <driver/rtc_io.h>

#include "sys_event_log.h"
#include "mona_file.h"
#include "pin_layout.h"


MONA_File::MONA_File() {
    this->_ref_counter++;
    if (this->_ref_counter == 1) {
        rtc_gpio_set_level((gpio_num_t)SD_EN, HIGH);
        delayMicroseconds(1000);
        SD_MMC.begin("/sdcard", true);
    }
}


MONA_File::~MONA_File() {
    this->_ref_counter--;
    if (this->_ref_counter == 0) {
        SD_MMC.end();
        rtc_gpio_set_level((gpio_num_t)SD_EN, LOW);
    }
}


bool MONA_File::append(const char *filename, const char *line) {
    file = SD_MMC.open(filename, FILE_APPEND);
    if (!file) {
        // This should be switched to event logging. But Bug #20213 prevents us from doing it now.
        Serial.println("Opening file failed.");
        // SYS_EVENT_LOGE("Opening file failed.");
        return false;
    }
    int bytes = file.print(line);
    file.close();
    return bytes+1;
}


bool MONA_File::read_line(const char *filename, char *out, int byte, int line_len) {
    file = SD_MMC.open(filename, FILE_READ);
    if (!file)
    {
        SYS_EVENT_LOGE("Opening file failed.");
        return false;
    }
    file.seek(byte);
    int bytes = file.readBytes(out, line_len);
    file.close();
    return bytes;
}

size_t MONA_File::file_size(const char *filename)
{
    file = SD_MMC.open(filename, FILE_READ);
    if (!file)
    {
        SYS_EVENT_LOGE("Opening file failed.");
        return -1;
    }
    size_t size = file.size();
    file.close();
    return size;
}

bool MONA_File::delete_file(const char *filename) {
    return SD_MMC.remove(filename);
}

