#pragma once

#include "SD_MMC.h"
#include "FS.h"
/**
 * @brief This class creates, deletes, reads and writes files. It is not possible to write
 * to arbitrary lines, because we only need to append text to a file.
 */
class MONA_File {
private:
    /**
     * @brief file object for firebeetle32 environment
     * 
     */
    File file;

    static inline int _ref_counter = 0;

public:
    /**
     * @brief constructor enables the sdcard through the corresponding gpio pin, waits 1000ms
     * and starts the communication over SD_MMC.
     */
    MONA_File();

    /**
     * @brief deconstructor end the communication over SD_MMC.
     */
    ~MONA_File();

    /**
     * @brief appends a new line to a file
     *
     * @param filename path to the file starting with '/'
     * @param line this string will be appended as a new line.
     *        A newline '\\n' at the end of `const char *line` is **not** automatically added!
     */
    bool append(const char *filename, const char *line);

    /**
     * @brief reads a line into a buffer
     * 
     * @note On the embedded environment the `line_len` is without the trailing `\x00`.
     * In case of a native test, the `line_len` has to be including `\x00`.
     *
     * @param filename path to the file starting with '/'
     * @param out the line will be read into this buffer
     * @param byte start byte of the line to read
     * @param line_len length of the line to read
     */
    bool read_line(const char *filename, char *out, int byte, int line_len);

    /**
     * @brief returns the size of the file
     *
     * @param filename path to the file starting with '/'
     */
    size_t file_size(const char *filename);
    
    /**
     * @brief deletes the file from sdcard
     *
     * @param filename path to the file starting with '/'
     */
    bool delete_file(const char *filename);

};

