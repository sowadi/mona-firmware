#ifndef SYS_EVENT_LOG
#define SYS_EVENT_LOG

/**
 * Enumeration to classify the message's severity.
 *  QUIET -> No log output
 *  ERROR -> Critical errors, software module can not recover on its own
 *  WARN  -> Error conditions from which recovery measures have been taken
 *  INFO  -> Information messages which describe normal flow of events
 *  DEBUG -> Extra information which is not necessary for normal use (values, pointers, sizes, etc).
 *  VERBOSE -> Bigger chunks of debugging information, or frequent messages which can potentially flood the output.
 * 
 * @note Default output levels: on SD Card is WARN(2) & for Serial is INFO(3)
 * 
 **/
#define FOREACH_LOGLVL(LOGLVL) \
    LOGLVL(QUIET)              \
    LOGLVL(ERROR)              \
    LOGLVL(WARN)               \
    LOGLVL(INFO)               \
    LOGLVL(DEBUG)              \
    LOGLVL(VERBOSE)

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

enum LOGLVL_ENUM
{
    FOREACH_LOGLVL(GENERATE_ENUM)
};

static const char *LOGLVL_STRING[] = {
    FOREACH_LOGLVL(GENERATE_STRING)};

#define SYS_EVENT_LOGE(msg_format, ...) sys_event_log(LOGLVL_ENUM::ERROR, __FILE__, __LINE__, __FUNCTION__, msg_format, ##__VA_ARGS__)
#define SYS_EVENT_LOGW(msg_format, ...) sys_event_log(LOGLVL_ENUM::WARN, __FILE__, __LINE__, __FUNCTION__, msg_format, ##__VA_ARGS__)
#define SYS_EVENT_LOGI(msg_format, ...) sys_event_log(LOGLVL_ENUM::INFO, __FILE__, __LINE__, __FUNCTION__, msg_format, ##__VA_ARGS__)
#define SYS_EVENT_LOGD(msg_format, ...) sys_event_log(LOGLVL_ENUM::DEBUG, __FILE__, __LINE__, __FUNCTION__, msg_format, ##__VA_ARGS__)
#define SYS_EVENT_LOGV(msg_format, ...) sys_event_log(LOGLVL_ENUM::VERBOSE, __FILE__, __LINE__, __FUNCTION__, msg_format, ##__VA_ARGS__)

#define LOG_FILE_PATH "/logs.log"

#include "mona_file.h"

void init_sys_event_log();


/**
 * @brief Function to write logs 
 *
 * This function is not intended to be used directly. Instead, use one of
 * SYS_EVENT_LOGE, SYS_EVENT_LOGW, SYS_EVENT_LOGI, SYS_EVENT_LOGD, SYS_EVENT_LOGV macros.
 * 
 */
void sys_event_log(LOGLVL_ENUM loglvl, const char *file, int line, const char *function, const char *msg_format, ...);

/**
 * @brief flush the log buffer to SD-card
 * This is necessary to reduce writes to the sd card since writing on every sys_event_log() statement would impair performance drastically
 */
void flush_buffer();



#endif